﻿using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateUserPermissionsBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public decimal User_Id { get; set; }
        public decimal Folder_Id { get; set; }
        public string List_Template { get; set; }
        public string Folder_Name { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }
    public class TEMTemplateUserPermissionsViewModel : TEMTemplateUserPermissionsBaseModel
    {
    }

    public class TEMTemplateUserPermissionsCreateModel 
    {
        public decimal User_Id { get; set; }
        public decimal Folder_Id { get; set; }
        public string List_Template { get; set; }
        public string Status { get; set; }
    }

    public class TEMTemplateUserPermissionsUpdateModel 
    {
        public decimal Id { get; set; }
        public decimal User_Id { get; set; }
        public decimal Folder_Id { get; set; }
        public string List_Template { get; set; }
        public string Status { get; set; }
    }
    public class TEMTemplateUserPermissionsDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMTemplateUserPermissionsApprovedModel
    {
        public decimal Id { get; set; }
    }
    public class TEMTemplateUserPermissionsQueryModel : PaginationRequest
    {
        public string DepartmentId { get; set; }
        public string FolderId { get; set; }
        public string Status { get; set; }
    }
}
