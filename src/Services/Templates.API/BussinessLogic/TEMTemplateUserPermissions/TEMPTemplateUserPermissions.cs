﻿using System;
using Utils;

namespace Templates.API.Infrastructure.Migrations
{
    public class TEMTemplateUserPermissions
    {
        public decimal Id { get; set; }
        public decimal User_Id { get; set; }
        public decimal Folder_Id { get; set; }
        public string List_Template { get; set; }

        // Default
        public string Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string ApproverBy { get; set; }
        public DateTime ApproverDate { get; set; }
    }
}
