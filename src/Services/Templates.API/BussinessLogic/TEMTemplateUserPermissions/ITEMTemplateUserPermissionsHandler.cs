﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMTemplateUserPermissionsHandler
    {

        Task<Response> GetAllAsync();
        Task<Response> GetbyUserIdAsync(decimal userId);
        Task<Response> GetById(decimal id);
        Task<Response> GetByFilterAsync(TEMTemplateUserPermissionsQueryModel filterModel);
        // 
        Task<Response> CreateAsync(List<TEMTemplateUserPermissionsCreateModel> models, BaseModel baseModel);
        Task<Response> UpdateAsync(List<TEMTemplateUserPermissionsUpdateModel> models, BaseModel baseModel);
        Task<Response> DeleteAsync(decimal id, BaseModel baseModel);
        Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel);
        Task<Response> DeleteByUserIdAsync(decimal userId);
        Task<Response> ApprovedAsync(TEMTemplateUserPermissionsApprovedModel model, BaseModel baseModel);
        Task<Response> NoApprovedAsync(TEMTemplateUserPermissionsApprovedModel model, BaseModel baseModel);
    }
}
