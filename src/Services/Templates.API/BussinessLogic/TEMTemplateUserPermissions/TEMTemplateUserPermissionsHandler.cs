﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Templates.API.Infrastructure.Migrations;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateUserPermissionsHandler : ITEMTemplateUserPermissionsHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMTemplateUserPermissions, TEMTemplateUserPermissionsViewModel, TEMTemplateUserPermissionsQueryModel> _TEMTemplateUserPermissionsRepositoryHandler = new RepositoryHandler<TEMTemplateUserPermissions, TEMTemplateUserPermissionsViewModel, TEMTemplateUserPermissionsQueryModel>();
        private readonly RepositoryHandler<TEMTemplateUserPermissionsBaseModel, TEMTemplateUserPermissionsViewModel, TEMTemplateUserPermissionsQueryModel> _TEMTemplateUserPermissionsVViewRepositoryHandler = new RepositoryHandler<TEMTemplateUserPermissionsBaseModel, TEMTemplateUserPermissionsViewModel, TEMTemplateUserPermissionsQueryModel>();

        private readonly ILogger<TEMTemplateUserPermissionsHandler> _logger;
        public TEMTemplateUserPermissionsHandler(ILogger<TEMTemplateUserPermissionsHandler> logger = null)
        {
            _logger = logger;
        }

        #region "Select Default Table"
        public async Task<Response> GetAllAsync()
        {
            try
            {
                var procName = "PKG_TEM_USER_PERMISISON.GET_ALL";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateUserPermissionsVViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> GetbyUserIdAsync(decimal userId)
        {
            try
            {
                var procName = "PKG_TEM_USER_PERMISISON.GET_BY_USERID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PUSERID", userId, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplateUserPermissionsVViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetById(decimal id)
        {
            try
            {
                var procName = "PKG_TEM_USER_PERMISISON.GET_BY_ID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplateUserPermissionsVViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> GetByFilterAsync(TEMTemplateUserPermissionsQueryModel filterModel)
        {
            try
            {
                var procName = "PKG_TEM_USER_PERMISISON.GET_BY_FILTER";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("pPageSize", filterModel.PageSize, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pPageIndex", filterModel.PageIndex, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pFullTextSearch", filterModel.FullTextSearch, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pStatus", (string.IsNullOrEmpty(filterModel.Status)) ? null : filterModel.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pDepartmentId", (string.IsNullOrEmpty(filterModel.DepartmentId)) ? null : filterModel.DepartmentId, OracleMappingType.Varchar2, ParameterDirection.Input);
                var response = await _TEMTemplateUserPermissionsVViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true) as ResponseObject<List<TEMTemplateUserPermissionsViewModel>>;
                
                return response;
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        #endregion

        #region "CRU Default Table"
        public async Task<Response> CreateAsync(List<TEMTemplateUserPermissionsCreateModel> models, BaseModel baseModel)
        {
            try
            {
                var listResult = new List<ResponseModel>();
                using (var unitOfWorkOracle = new UnitOfWorkOracle())
                {
                    var iConn = unitOfWorkOracle.GetConnection();
                    var iTrans = iConn.BeginTransaction();
                    try
                    {
                       
                        if (models != null && models.Count > 0)
                        {
                            bool check = true;
                            foreach (var model in models)
                            {
                                var listQuery = await GetbyUserIdAsync(model.User_Id) as ResponseObject<List<TEMTemplateUserPermissionsViewModel>>;
                                if (listQuery != null && listQuery.Data != null && listQuery.Data.Count() > 0)
                                {
                                    iTrans.Rollback();
                                    return new ResponseError(StatusCode.Fail, "Người dùng " + listQuery.Data.FirstOrDefault().FullName + " đã tồn tại quyền");
                                }

                                var procName = "PKG_TEM_USER_PERMISISON.INSERT_ROW";
                                var dyParam = new OracleDynamicParameters();
                                dyParam.Add("PUSER_ID ", model.User_Id, OracleMappingType.Varchar2, ParameterDirection.Input);
                                dyParam.Add("PFOLDER_ID ", model.Folder_Id, OracleMappingType.Varchar2, ParameterDirection.Input);
                                dyParam.Add("PLIST_TEMPLATE ", model.List_Template, OracleMappingType.Varchar2, ParameterDirection.Input);
                                dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                                dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                                var createResult = await _TEMTemplateUserPermissionsRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                                if (createResult != null)
                                {
                                    listResult.Add(new ResponseModel
                                    {
                                        Id = createResult.Data.Id,
                                        Name = createResult.Data.Name,
                                        Status = createResult.Data.Status,
                                        Message = (createResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                                    });
                                    if (!createResult.Data.Status.Equals("00"))
                                    {
                                        check = false;
                                    }
                                }
                            }
                            if (check)
                            {
                                iTrans.Commit();
                                return new ResponseObject<List<ResponseModel>>(listResult, "Thành công", StatusCode.Success);
                            }
                            else
                            {
                                iTrans.Rollback();
                                return new ResponseObject<List<ResponseModel>>(listResult, "Thất bại", StatusCode.Fail);
                            }
                        }
                        else
                        {
                            return new ResponseObject<List<ResponseModel>>(listResult, "Dữ liệu không tồn tại!", StatusCode.Fail);
                        }
                    }
                    catch (Exception ex)
                    {
                        iTrans.Rollback();
                        if (_logger != null)
                        {
                            _logger.LogError(ex, "Exception Error");
                            return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                        }
                        else throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> UpdateAsync(List<TEMTemplateUserPermissionsUpdateModel> models, BaseModel baseModel)
        {

            try
            {
                var listResult = new List<ResponseModel>();
                using (var unitOfWorkOracle = new UnitOfWorkOracle())
                {
                    var iConn = unitOfWorkOracle.GetConnection();
                    var iTrans = iConn.BeginTransaction();
                    try
                    {

                        if (models != null && models.Count > 0)
                        {
                           bool check = true;
                            var procName = "PKG_TEM_USER_PERMISISON.DELETE_BY_USERID";
                            var dyParam = new OracleDynamicParameters();
                            dyParam.Add("PUSERID ", models.FirstOrDefault().User_Id, OracleMappingType.Varchar2, ParameterDirection.Input);
                            dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                            var deleteResult = await _TEMTemplateUserPermissionsRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;
                            if (deleteResult != null)
                            {
                                if (!deleteResult.Data.Status.Equals("00"))
                                {
                                    return new ResponseObject<List<ResponseModel>>(listResult, "Không xóa được các bản ghi cũ.");
                                }
                                foreach (var model in models)
                                {
                                    procName = "PKG_TEM_USER_PERMISISON.INSERT_ROW";
                                    dyParam = new OracleDynamicParameters();
                                    dyParam.Add("PUSER_ID ", model.User_Id, OracleMappingType.Varchar2, ParameterDirection.Input);
                                    dyParam.Add("PFOLDER_ID ", model.Folder_Id, OracleMappingType.Varchar2, ParameterDirection.Input);
                                    dyParam.Add("PLIST_TEMPLATE ", model.List_Template, OracleMappingType.Varchar2, ParameterDirection.Input);
                                    dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                                    dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                                    var createResult = await _TEMTemplateUserPermissionsRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;
                                    if (createResult != null)
                                    {
                                        listResult.Add(new ResponseModel
                                        {
                                            Id = createResult.Data.Id,
                                            Name = createResult.Data.Name,
                                            Status = createResult.Data.Status,
                                            Message = (createResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                                        });
                                        if (!createResult.Data.Status.Equals("00"))
                                        {
                                            check = false;
                                        }
                                    }
                                    
                                }

                                if (check)
                                {
                                    iTrans.Commit();
                                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công", StatusCode.Success);
                                }
                                else
                                {
                                    iTrans.Rollback();
                                    return new ResponseObject<List<ResponseModel>>(listResult, "Thất bại", StatusCode.Fail);
                                }
                            }
                            else
                            {
                                return new ResponseObject<List<ResponseModel>>(listResult, "Không xóa được các bản ghi cũ.");
                            }
                        }
                        else
                        {
                            return new ResponseObject<List<ResponseModel>>(listResult, "Dữ liệu không tồn tại");
                        }
                    }
                    catch (Exception ex)
                    {
                        iTrans.Rollback();
                        if (_logger != null)
                        {
                            _logger.LogError(ex, "Exception Error");
                            return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                        }
                        else throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteAsync(decimal id, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_USER_PERMISISON.DELETE_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                return await _TEMTemplateUserPermissionsRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel)
        {
            var listResult = new List<ResponseModel>();
            using (var unitOfWorkOracle = new UnitOfWorkOracle())
            {
                var iConn = unitOfWorkOracle.GetConnection();
                var iTrans = iConn.BeginTransaction();
                if (listId != null && listId.Count > 0)
                {
                    foreach (var id in listId)
                    {
                        var procName = "PKG_TEM_USER_PERMISISON.DELETE_ROW";
                        var dyParam = new OracleDynamicParameters();
                        dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                        dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                        dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                        var deleteResult = await _TEMTemplateUserPermissionsRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                        if (deleteResult != null)
                        {
                            listResult.Add(new ResponseModel
                            {
                                Id = deleteResult.Data.Id,
                                Name = deleteResult.Data.Name,
                                Status = deleteResult.Data.Status,
                                Message = (deleteResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                            });
                        }
                    }
                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
                else
                {
                    return new ResponseObject<List<ResponseModel>>(listResult, "Không thành công");
                }

            }

        }

        public async Task<Response> DeleteByUserIdAsync(decimal userId)
        {
            try
            {
                var procName = "PKG_TEM_USER_PERMISISON.DELETE_BY_USERID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PUSERID ", userId, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                return await _TEMTemplateUserPermissionsRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> ApprovedAsync(TEMTemplateUserPermissionsApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetById(model.Id) as ResponseObject<TEMTemplateUserPermissionsViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_USER_PERMISISON.APPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMTemplateUserPermissionsRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> NoApprovedAsync(TEMTemplateUserPermissionsApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetById(model.Id) as ResponseObject<TEMTemplateUserPermissionsViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_USER_PERMISISON.NOAPPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMTemplateUserPermissionsRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        #endregion
    }
}
