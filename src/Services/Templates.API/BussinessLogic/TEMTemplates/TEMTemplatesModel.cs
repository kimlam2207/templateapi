﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplatesBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Folder_Id { get; set; }
        public string FolderName { get; set; }
        public string StatusView { get; set; }
        public string StatusCode { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }

    public class TEMTemplatesCreateModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Folder_Id { get; set; }
        public string Status { get; set; }
        public DateTime Effective_Start_Date { get; set; }
        public DateTime Effective_End_Date { get; set; }
        public string Description { get; set; }
        public string Param_Data_Json { get; set; }
        public IFormFile Files { get; set; }
        public List<string> Aplications { get; set; }
        public List<string> TemplateTypes { get; set; }

    }

    public class TEMTemplatesUpdateModel
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Folder_Id { get; set; }
        public string Status { get; set; }
        public DateTime Effective_Start_Date { get; set; }
        public DateTime Effective_End_Date { get; set; }
        public string Description { get; set; }
        public string Param_Data_Json { get; set; }
        public List<string> Aplications { get; set; }
        public List<string> TemplateTypes { get; set; }
        public decimal TemplateVersionId { get; set; }
        public IFormFile Files { get; set; } 


    }

    public class TEMTemplatesDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMTemplatesApprovedModel
    {
        public decimal Id { get; set; }
        public decimal TemplateVersionId { get; set; }
    }
    public class TEMTemplatesNoApprovedModel
    {
        public decimal Id { get; set; }
        public decimal TemplateVersionId { get; set; }
    }

    public class TEMTemplatesQueryModel : PaginationRequest
    {
        public string Status { get; set; }
        public string Name { get; set; }
        public string Folder { get; set; }
        public string Aplication { get; set; }
        public string UserId { get; set; }
    }

    public class TEMTemplatesViewModel : TEMTemplatesBaseModel
    {
        public List<TEMTemplateVersionsViewModel> Versions { get; set; } = new List<TEMTemplateVersionsViewModel>();
    }
    public class TEMTemplatesCacheModel
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Folder_Id { get; set; }
    }

      public class TEMTemplatesReplaceDataModel
    {
        public decimal TemplateId { get; set; }
        public decimal TemplateVersionId { get; set; }
        public decimal AplicationId { get; set; }
        public string  Token { get; set; }
        public string  DataJson { get; set; }
        public decimal TemplateTypeId { get; set; }
    }

    public class TEMTemplatesReplaceDataNewModel
    {
        public string TemplateCode { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string AplicationCode { get; set; }
        public string Token { get; set; }
        public string DataJson { get; set; }
        public string TemplateTypeCode { get; set; }
    }
}
