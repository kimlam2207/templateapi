﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Templates.API.Infrastructure.Migrations;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using DocumentFormat.OpenXml.EMMA;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplatesHandler : ITEMTemplatesHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMTemplates, TEMTemplatesBaseModel, TEMTemplatesQueryModel> _TEMTemplatesRepositoryHandler = new RepositoryHandler<TEMTemplates, TEMTemplatesBaseModel, TEMTemplatesQueryModel>();
        private readonly RepositoryHandler<TEMTemplatesBaseModel, TEMTemplatesViewModel, TEMTemplatesQueryModel> _TEMTemplatesViewRepositoryHandler = new RepositoryHandler<TEMTemplatesBaseModel, TEMTemplatesViewModel, TEMTemplatesQueryModel>();
        //private readonly TEMTemplateVersionsHandler _TEMTemplateVersionsInterfaceHandler = new TEMTemplateVersionsHandler();
        private readonly ITEMTemplateVersionsHandler _TEMTemplateVersionsInterfaceHandler;
        private readonly ILogger<TEMTemplatesHandler> _logger;
        private readonly IRedisService _redisService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ITEMTemplateTypeMapsHandler _TEMTemplateTypeMapsInterfaceHandler;
        private readonly ITEMTemplateAplicationPermissionsHandler _TEMTemplateAplicationPermissionsHandler;
        private readonly ITEMAplicationsHandler _TEMAplicationsHandler;
        private readonly ITEMTemplatesTypeHandler _TEMTemplatesTypeHandler;
        private readonly string _contentRootPath;

        public TEMTemplatesHandler(ILogger<TEMTemplatesHandler> logger = null, 
                                    ITEMTemplateVersionsHandler templateVersionsInterfaceHandler=null,
                                    IRedisService redisService=null,
                                    IHostingEnvironment hostingEnvironment=null,
                                    ITEMTemplateTypeMapsHandler templateTypeMapsInterfaceHandler=null,
                                    ITEMTemplateAplicationPermissionsHandler templateAplicationPermissionsHandler=null,
                                    ITEMAplicationsHandler aplicationsHandler=null,
                                    ITEMTemplatesTypeHandler templatesTypeHandler=null
                                    )
        {
            _logger = logger;
            _TEMTemplateVersionsInterfaceHandler = templateVersionsInterfaceHandler;
            _redisService = redisService;
            _hostingEnvironment = hostingEnvironment;
            _contentRootPath = hostingEnvironment.ContentRootPath;
            _TEMTemplateTypeMapsInterfaceHandler = templateTypeMapsInterfaceHandler;
            _TEMTemplateAplicationPermissionsHandler = templateAplicationPermissionsHandler;
            _TEMAplicationsHandler = aplicationsHandler;
            _TEMTemplatesTypeHandler = templatesTypeHandler;
        }
      

        #region "Select Default Table"
        public async Task<Response> GetAllAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE.GET_ALL";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetAllActiveAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE.GET_ALL_ACTIVE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByIdAsync(decimal id)
        {
            try
            {
                var cacheKey = Helpers.GetConfig("Redis:Key") + "TEMTemplates" + Convert.ToInt32(id);
                var cachedValue = await _redisService.GetFromCache<TEMTemplatesViewModel>(cacheKey);
                if (cachedValue != null)
                {
                    ResponseObject<TEMTemplatesViewModel> resultCache = new ResponseObject<TEMTemplatesViewModel>(cachedValue);
                    resultCache.StatusCode = Utils.StatusCode.Success;
                    resultCache.Message = "Thành công";
                    return resultCache;
                }

                var procName = "PKG_TEM_TEMPLATE.GET_BY_ID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByCodeAsync(decimal id, string name)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE.GET_BY_CODE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PCODE", name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByFilterAsync(TEMTemplatesQueryModel filterModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE.GET_BY_FILTER";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("pPageSize", filterModel.PageSize, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pPageIndex", filterModel.PageIndex, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pFullTextSearch", filterModel.FullTextSearch, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pName", (string.IsNullOrEmpty(filterModel.Name)) ? null : filterModel.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pStatus", (string.IsNullOrEmpty(filterModel.Status)) ? null : filterModel.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pFolder", (string.IsNullOrEmpty(filterModel.Folder)) ? null : filterModel.Folder, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pAplication", (string.IsNullOrEmpty(filterModel.Aplication)) ? null : filterModel.Aplication, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pUserId", (string.IsNullOrEmpty(filterModel.UserId)) ? null : filterModel.UserId, OracleMappingType.Varchar2, ParameterDirection.Input);
                var response = await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true) as ResponseObject<List<TEMTemplatesViewModel>>;
                if (response!=null && response.Data!=null && response.Data.Count()>0)
                {
                    foreach (var item in response.Data)
                    {
                        var responseTemplateVersion = await _TEMTemplateVersionsInterfaceHandler.GetByTemplateIdAsync(item.Id) as ResponseObject<List<TEMTemplateVersionsViewModel>>;
                        if (responseTemplateVersion!=null && responseTemplateVersion.Data!=null && responseTemplateVersion.Data.Count()>0)
                        {
                            item.Versions = responseTemplateVersion.Data;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        #endregion

        #region "CRU Default Table"
        public async Task<Response> CreateAsync(TEMTemplatesCreateModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByCodeAsync(0, model.Code) as ResponseObject<TEMTemplatesViewModel>;
                if (objQuery != null && objQuery.Data != null)
                {
                    return new ResponseError(StatusCode.Fail, "Mã code đã tồn tại");
                }

                //lưu file vào ECM
                ResponseECM responseUploadECM = await ECM.UploadFileToECM(model.Files, _contentRootPath);
                if (responseUploadECM!=null && responseUploadECM.StatusCode == Utils.StatusCode.Success && responseUploadECM.properties!=null)
                {
                    //lưu template 
                    var procName = "PKG_TEM_TEMPLATE.INSERT_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PCODE", model.Code, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PNAME", model.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PFOLDERID", model.Folder_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                    var responseTemplate = await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracle(procName, dyParam) as ResponseObject<ResponseModel>;
                    if (responseTemplate != null && responseTemplate?.Data != null && responseTemplate.Data.Status == "00")
                    {
                        //lưu template vertion
                        TEMTemplateVersionsCreateModel templateVersion = new TEMTemplateVersionsCreateModel()
                        {
                            Template_Id = responseTemplate.Data.Id,
                            Doc_Temp_Id = responseUploadECM.properties[0][0].id,
                            Template_Name = model.Files.FileName,
                            Template_ContentType = model.Files.ContentType,
                            Param_Data_Json = model.Param_Data_Json,
                            Effective_Start_Date = model.Effective_Start_Date,
                            Effective_End_Date = model.Effective_End_Date,
                            Description = model.Description
                        };
                        var responseTemplateVersion = await _TEMTemplateVersionsInterfaceHandler.CreateAsync(templateVersion, baseModel) as ResponseObject<ResponseModel>;

                        SetRedisTemplate(responseTemplate.Data.Id);
                        //set cache lên redis với bảng template version                     
                        _TEMTemplateVersionsInterfaceHandler.SetRedisTemplateVersion(responseTemplateVersion.Data.Id);

                        //insert bảng type map
                        if (model.TemplateTypes!=null  && model.TemplateTypes.Count()>0)
                        {
                            TEMTemplateTypeMapsCreateListModel templateTypeMap = new TEMTemplateTypeMapsCreateListModel();
                            templateTypeMap.Template_Id=responseTemplate.Data.Id;
                            templateTypeMap.TemplateTypes = model.TemplateTypes;
                            await _TEMTemplateTypeMapsInterfaceHandler.CreateListAsync(templateTypeMap, baseModel);
                        }

                        //insert bảng application permission
                        if (model.Aplications!=null && model.Aplications.Count()>0)
                        {
                            TEMTemplateAplicationPermissionsListModel templateAplicationPermissionList = new TEMTemplateAplicationPermissionsListModel();
                            templateAplicationPermissionList.TemplateId = responseTemplate.Data.Id;
                            templateAplicationPermissionList.Aplications = model.Aplications;
                            await _TEMTemplateAplicationPermissionsHandler.CreateListAsync(templateAplicationPermissionList, baseModel);
                        }


                    }
                    return responseTemplate;
                }
                else
                {
                    return new ResponseError(StatusCode.Fail, responseUploadECM.Message);
                }

            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, ex.Message);
                }
                else throw ex;
            }
        }
        public async Task<Response> UpdateAsync(TEMTemplatesUpdateModel model, BaseModel baseModel)
        {
            try
            {
                ResponseObject<ResponseModel> responseTemplate = null;
                ResponseECM responseUploadECM = new ResponseECM();
                var options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(Convert.ToInt32(Helpers.GetConfig("Redis:Expired"))));

                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplatesViewModel>;
                var objQueryTemplateVersion = await _TEMTemplateVersionsInterfaceHandler.GetByIdAsync(model.TemplateVersionId) as ResponseObject<TEMTemplateVersionsViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    objQuery = await GetByCodeAsync(model.Id, model.Code) as ResponseObject<TEMTemplatesViewModel>;
                    if (objQuery != null && objQuery.Data != null)
                    {
                        return new ResponseError(StatusCode.Fail, "Mã code đã tồn tại");
                    }

                    var templateVersionListByTemplateId= await _TEMTemplateVersionsInterfaceHandler.GetByTemplateIdAsync(model.Id) as ResponseObject<List<TEMTemplateVersionsViewModel>>;
                    if (templateVersionListByTemplateId.Data.Count>1  || 
                        (templateVersionListByTemplateId.Data.Count == 1 && templateVersionListByTemplateId.Data.FirstOrDefault().Status!="DEACTIVE")
                     )
                    {
                        var templateVersionByEffectiveStartDate = templateVersionListByTemplateId.Data.Where(s => s.Effective_Start_Date.Date <= model.Effective_Start_Date && s.Effective_End_Date >= model.Effective_Start_Date).ToList();
                        var templateVersionByEffectiveEndDate = templateVersionListByTemplateId.Data.Where(s => s.Effective_Start_Date.Date <= model.Effective_End_Date && s.Effective_End_Date >= model.Effective_End_Date).ToList();
                        if (templateVersionByEffectiveStartDate.Count()>0 || templateVersionByEffectiveEndDate.Count()>0)
                        {
                            return new ResponseError(StatusCode.Fail, "Thời gian bắt đầu hoặc thời gian kết thúc đã nằm trong khoảng thời gian của một version khác thuộc template này");
                        }
                    }


                    if (model.Files != null && model.Files.Length>0)
                    {
                        //lưu file vào ECM
                        responseUploadECM = await ECM.UploadFileToECM(model.Files, _contentRootPath);
                        if (responseUploadECM==null || responseUploadECM.StatusCode==StatusCode.Fail)
                        {
                            return new ResponseError(StatusCode.Fail, responseUploadECM.Message);
                        }
                    }
                    var procNam = "PKG_TEM_TEMPLATE.UPDATE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PCODE", model.Code, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PNAME", model.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PFOLDERID", model.Folder_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("LASTMODIFIEDBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                    if (model.Files != null && model.Files.Length > 0 && responseUploadECM!=null && responseUploadECM.StatusCode==StatusCode.Success && responseUploadECM.properties!=null)
                    {
                        responseTemplate = await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam) as ResponseObject<ResponseModel>;
                        if (responseTemplate != null && responseTemplate?.Data != null && responseTemplate.Data.Status == "00")
                        {
                            //set cache lên redis với bảng template
                            //
                            SetRedisTemplate(model.Id);
                            if (objQueryTemplateVersion.Data.Status== "DEACTIVE")
                            {
                                TEMTemplateVersionsUpdateModel templateVersion = new TEMTemplateVersionsUpdateModel()
                                {
                                    Id=model.TemplateVersionId,
                                    Template_Id = model.Id,
                                    Doc_Temp_Id = responseUploadECM.properties[0][0].id,
                                    Template_Name = model.Files.FileName,
                                    Template_ContentType = model.Files.ContentType,
                                    Param_Data_Json = model.Param_Data_Json,
                                    Effective_Start_Date = model.Effective_Start_Date,
                                    Effective_End_Date = model.Effective_End_Date,
                                    Description = model.Description
                                };
                              
                                await _TEMTemplateVersionsInterfaceHandler.UpdateAsync(templateVersion,baseModel);

                                _TEMTemplateVersionsInterfaceHandler.SetRedisTemplateVersion(templateVersion.Id);

                            }
                            else
                            {
                                TEMTemplateVersionsCreateModel templateVersion = new TEMTemplateVersionsCreateModel()
                                {
                                    Template_Id = model.Id,
                                    Doc_Temp_Id = responseUploadECM.properties[0][0].id,
                                    Template_Name = model.Files.FileName,
                                    Template_ContentType = model.Files.ContentType,
                                    Param_Data_Json = model.Param_Data_Json,
                                    Effective_Start_Date = model.Effective_Start_Date,
                                    Effective_End_Date = model.Effective_End_Date,
                                    Description = model.Description
                                };

                                var responseTemplateVersion = await _TEMTemplateVersionsInterfaceHandler.CreateAsync(templateVersion, baseModel) as ResponseObject<ResponseModel>;
                                //set cache lên redis với bảng template version
                                _TEMTemplateVersionsInterfaceHandler.SetRedisTemplateVersion(responseTemplateVersion.Data.Id);

                            }


                        }
                        else
                        {
                            return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                        }
                    }
                    else if(model.Files == null || model.Files.Length==0)
                    {
                        responseTemplate = await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam) as ResponseObject<ResponseModel>;
                        if (responseTemplate != null && responseTemplate?.Data != null && responseTemplate.Data.Status == "00")
                        {
                            SetRedisTemplate(model.Id);
                        }
                        else
                        {
                            return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                        }

                        TEMTemplateVersionsUpdateModel templateVersion = new TEMTemplateVersionsUpdateModel()
                        {
                            Id = model.TemplateVersionId,
                            Template_Id = model.Id,
                            Doc_Temp_Id = objQueryTemplateVersion.Data.Doc_Temp_Id,
                            Template_Name = objQueryTemplateVersion.Data.Template_Name,
                            Template_ContentType = objQueryTemplateVersion.Data.Template_ContentType,
                            Param_Data_Json = model.Param_Data_Json,
                            Effective_Start_Date = model.Effective_Start_Date,
                            Effective_End_Date = model.Effective_End_Date,
                            Description = model.Description
                        };

                        await _TEMTemplateVersionsInterfaceHandler.UpdateAsync(templateVersion, baseModel);
                        _TEMTemplateVersionsInterfaceHandler.SetRedisTemplateVersion(templateVersion.Id);
                    }

                    if (responseTemplate.StatusCode==StatusCode.Success)
                    {
                        //insert bảng type map
                        if (model.TemplateTypes != null && model.TemplateTypes.Count() > 0)
                        {
                            TEMTemplateTypeMapsCreateListModel templateTypeMap = new TEMTemplateTypeMapsCreateListModel();
                            templateTypeMap.Template_Id = responseTemplate.Data.Id;
                            templateTypeMap.TemplateTypes = model.TemplateTypes;
                            await _TEMTemplateTypeMapsInterfaceHandler.CreateListAsync(templateTypeMap, baseModel);
                        }

                        //insert bảng application permission
                        if (model.Aplications != null && model.Aplications.Count() > 0)
                        {
                            TEMTemplateAplicationPermissionsListModel templateAplicationPermissionList = new TEMTemplateAplicationPermissionsListModel();
                            templateAplicationPermissionList.TemplateId = model.Id;
                            templateAplicationPermissionList.Aplications = model.Aplications;
                            await _TEMTemplateAplicationPermissionsHandler.CreateListAsync(templateAplicationPermissionList, baseModel);

                        }

                    }
                    return responseTemplate;
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteAsync(decimal id, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE.DELETE_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                var objQuery = await GetByIdAsync(id) as ResponseObject<TEMTemplatesViewModel>;

                var response= await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracle(procName, dyParam) as ResponseObject<ResponseModel>;
                if (response!=null && response.Data!=null && response.Data.Status == "00")
                {
                    SetRedisTemplate(response.Data.Id);
                }
                return response;
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel)
        {
            var listResult = new List<ResponseModel>();
            using (var unitOfWorkOracle = new UnitOfWorkOracle())
            {
                var iConn = unitOfWorkOracle.GetConnection();
                var iTrans = iConn.BeginTransaction();
                if (listId != null && listId.Count > 0)
                {
                    foreach (var id in listId)
                    {
                        var procName = "PKG_TEM_TEMPLATE.DELETE_ROW";
                        var dyParam = new OracleDynamicParameters();
                        dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                        dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                        dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                        var deleteResult = await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                        if (deleteResult != null)
                        {
                            SetRedisTemplate(deleteResult.Data.Id);
                            listResult.Add(new ResponseModel
                            {
                                Id = deleteResult.Data.Id,
                                Name = deleteResult.Data.Name,
                                Status = deleteResult.Data.Status,
                                Message = (deleteResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                            });
                        }
                    }
                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
                else
                {
                    return new ResponseObject<List<ResponseModel>>(listResult, "Không thành công");
                }

            }

        }
        public async Task<Response> ApprovedAsync(TEMTemplatesApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(Convert.ToInt32(Helpers.GetConfig("Redis:Expired"))));

                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplatesViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMPLATE.APPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    if (objQuery.Data.Status == "ACTIVE")
                    {
                        TEMTemplateVersionsApprovedModel modelTemplateVersion = new TEMTemplateVersionsApprovedModel() {
                            Id = model.TemplateVersionId
                        };

                        var response = await _TEMTemplateVersionsInterfaceHandler.ApprovedAsync(modelTemplateVersion,baseModel) as ResponseObject<ResponseModel>;
                        if (response != null && response.StatusCode == StatusCode.Success)
                        {
                            _TEMTemplateVersionsInterfaceHandler.SetRedisTemplateVersion(model.TemplateVersionId);
                        }
                        return response;
                    }
                    else
                    {
                        TEMTemplateVersionsApprovedModel modelTemplateVersion = new TEMTemplateVersionsApprovedModel()
                        {
                            Id = model.TemplateVersionId
                        };

                        var response =await _TEMTemplateVersionsInterfaceHandler.ApprovedAsync(modelTemplateVersion, baseModel) as ResponseObject<ResponseModel>;
                        if (response!=null && response.StatusCode==StatusCode.Success)
                        {
                            _TEMTemplateVersionsInterfaceHandler.SetRedisTemplateVersion(model.TemplateVersionId);
                            await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                            SetRedisTemplate(model.Id);
                        }
                        return response;
                    }

                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> NoApprovedAsync(TEMTemplatesNoApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplatesViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMPLATE.NOAPPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    if (objQuery.Data.Status == "ACTIVE")
                    {
                        TEMTemplateVersionsNoApprovedModel modelTemplateVersion = new TEMTemplateVersionsNoApprovedModel()
                        {
                            Id = model.TemplateVersionId
                        };

                        var response = await _TEMTemplateVersionsInterfaceHandler.NoApprovedAsync(modelTemplateVersion, baseModel) as ResponseObject<ResponseModel>;
                        if (response != null && response.StatusCode == StatusCode.Success)
                        {
                            _TEMTemplateVersionsInterfaceHandler.SetRedisTemplateVersion(model.TemplateVersionId);
                        }
                        return response;
                    }
                    else
                    {
                        TEMTemplateVersionsNoApprovedModel modelTemplateVersion = new TEMTemplateVersionsNoApprovedModel()
                        {
                            Id = model.TemplateVersionId
                        };

                        var response = await _TEMTemplateVersionsInterfaceHandler.NoApprovedAsync(modelTemplateVersion, baseModel) as ResponseObject<ResponseModel>;
                        if (response != null && response.StatusCode == StatusCode.Success)
                        {
                            _TEMTemplateVersionsInterfaceHandler.SetRedisTemplateVersion(model.TemplateVersionId);
                            await _TEMTemplatesViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam); SetRedisTemplate(model.Id);
                        }
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<ResponseECM> RenderFileAsync(TEMTemplatesReplaceDataNewModel model, BaseModel baseModel, IECMHandler _ECMHandler)
        {
            ResponseECM response = new ResponseECM();
            try
            {
                //kiểm template
                var template = await GetByCodeAsync(0,model.TemplateCode) as ResponseObject<TEMTemplatesViewModel>;
                if (template==null || template.Data==null)
                {
                    response.Message = "Template không tồn tại.";
                    response.StatusCode = StatusCode.Fail;
                }
                //kiểm tra ứng dụng
                var aplication = await _TEMAplicationsHandler.GetByCodeAsync(0,model.AplicationCode) as ResponseObject<TEMAplicationsViewModel>;
                if (aplication == null || aplication.Data == null)
                {
                    response.Message = "Ứng dụng không tồn tại.";
                    response.StatusCode = StatusCode.Fail;
                }
                //kiểm tra template type
                var templateType = await _TEMTemplatesTypeHandler.GetByCodeAsync(0,model.TemplateTypeCode) as ResponseObject<TEMTemplateTypesViewModel>;
                if (templateType == null || templateType.Data == null)
                {
                    response.Message = "Loại xuất không tồn tại.";
                    response.StatusCode = StatusCode.Fail;
                }

                //kiểm tra version template
                var templateVersions = await _TEMTemplateVersionsInterfaceHandler.GetByTemplateIdAsync(template.Data.Id) as ResponseObject<List<TEMTemplateVersionsViewModel>>;
                var templateVersion = templateVersions.Data.Where(s => s.Effective_Start_Date.Date <= model.EffectiveDate && s.Effective_End_Date >= model.EffectiveDate).ToList();
                if (templateVersion == null || templateVersion.Count()==0)
                {
                    response.Message = "Không có version nào được sử dụng trong khoảng thời gian " + model.EffectiveDate.ToString("dd/MM/yyyy");
                    response.StatusCode = StatusCode.Fail;
                }

                if (response.StatusCode==StatusCode.Success)
                {
                    TEMTemplatesReplaceDataModel renderModel = new TEMTemplatesReplaceDataModel()
                    {
                        TemplateId = template.Data.Id,
                        TemplateVersionId = templateVersion.FirstOrDefault().Id,
                        AplicationId = aplication.Data.Id,
                        Token = model.Token,
                        DataJson = model.DataJson,
                        TemplateTypeId = templateType.Data.Id
                    };
                    response = await _ECMHandler.RenderFileAsync(renderModel,baseModel);
                }             
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.StatusCode = StatusCode.Fail;
            }
            return response;
        }

        #endregion

        #region Redis
        public async void SetRedisTemplate(decimal id)
        {
            var options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(Convert.ToInt32(Helpers.GetConfig("Redis:Expired"))));

            //set cache lên redis với bảng template

            var cacheKey = Helpers.GetConfig("Redis:Key") + "TEMTemplates" + Convert.ToInt32(id);
            //remove cache cũ
            await _redisService.ClearCache(cacheKey);
            var modelCacheTemplate = await GetByIdAsync(id) as ResponseObject<TEMTemplatesViewModel>;

            // cachedTime = "Expired";
            // Nạp  giá trị mới cho cache
            await _redisService.SetCache<TEMTemplatesViewModel>(cacheKey, modelCacheTemplate.Data, options);
        }
        #endregion
    }
}
