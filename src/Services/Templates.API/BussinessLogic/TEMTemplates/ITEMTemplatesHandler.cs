﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMTemplatesHandler
    {
        //Get
        Task<Response> GetAllAsync();
        Task<Response> GetAllActiveAsync();
        Task<Response> GetByIdAsync(decimal id);
        Task<Response> GetByCodeAsync(decimal id, string code);
        Task<Response> GetByFilterAsync(TEMTemplatesQueryModel filterModel);
        //CRUD
        Task<Response> CreateAsync(TEMTemplatesCreateModel model, BaseModel baseModel);
        Task<Response> UpdateAsync(TEMTemplatesUpdateModel model, BaseModel baseModel);
        Task<Response> DeleteAsync(decimal id, BaseModel baseModel);
        Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel);
        Task<Response> ApprovedAsync(TEMTemplatesApprovedModel model, BaseModel baseModel);

        Task<Response> NoApprovedAsync(TEMTemplatesNoApprovedModel model, BaseModel baseModel);

        Task<ResponseECM> RenderFileAsync(TEMTemplatesReplaceDataNewModel model, BaseModel baseModel, IECMHandler _ECMHandler);
    }
}
