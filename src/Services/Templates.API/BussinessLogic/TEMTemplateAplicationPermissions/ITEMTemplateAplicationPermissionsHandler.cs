﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMTemplateAplicationPermissionsHandler
    {
        //Get
        Task<Response> GetAllAsync();
        Task<Response> GetByIdAsync(decimal id);
        Task<Response> GetByTemplateIdOrAplicationIdAsync(decimal templateId, decimal aplicationId);

        //CRUD
        Task<Response> CreateAsync(TEMTemplateAplicationPermissionsCreateModel model, BaseModel baseModel);
        Task<Response> UpdateAsync(TEMTemplateAplicationPermissionsUpdateModel model, BaseModel baseModel);
        Task<Response> DeleteAsync(decimal id, BaseModel baseModel);
        Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel);
        Task<Response> ApprovedAsync(TEMTemplateAplicationPermissionsApprovedModel model, BaseModel baseModel);
        Task<Response> CreateListAsync(TEMTemplateAplicationPermissionsListModel model, BaseModel baseModel);
    }
}
