﻿using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateAplicationPermissionsBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Aplication_Id { get; set; }
        public string StatusView { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }

    public class TEMTemplateAplicationPermissionsCreateModel
    {
        public decimal Template_Id { get; set; }
        public decimal Aplication_Id { get; set; }
        public string Status { get; set; }
    }

    public class TEMTemplateAplicationPermissionsListModel
    {
        public List<string> Aplications { get; set; }
        public decimal TemplateId { get; set; }
    }

    public class TEMTemplateAplicationPermissionsUpdateModel
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Aplication_Id { get; set; }
        public string Status { get; set; }
    }
    public class TEMTemplateAplicationPermissionsDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMTemplateAplicationPermissionsApprovedModel
    {
        public decimal Id { get; set; }
    }
    public class TEMTemplateAplicationPermissionsQueryModel : PaginationRequest
    {
        public string Status { get; set; }
    }

    public class TEMTemplateAplicationPermissionsViewModel : TEMTemplateAplicationPermissionsBaseModel
    {
        public bool IsRemove { get; set; } = true;
    }
}
