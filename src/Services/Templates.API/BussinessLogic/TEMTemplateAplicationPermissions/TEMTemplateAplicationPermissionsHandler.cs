﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Templates.API.Infrastructure.Migrations;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;
using TTemplates.API.BussinessLogic;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateAplicationPermissionsHandler : ITEMTemplateAplicationPermissionsHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMTemplateAplicationPermissions, TEMTemplateAplicationPermissionsBaseModel, TEMTemplateAplicationPermissionsQueryModel> _TEMTemplateAplicationPermissionsRepositoryHandler = new RepositoryHandler<TEMTemplateAplicationPermissions, TEMTemplateAplicationPermissionsBaseModel, TEMTemplateAplicationPermissionsQueryModel>();
        private readonly RepositoryHandler<TEMTemplateAplicationPermissionsBaseModel, TEMTemplateAplicationPermissionsViewModel, TEMTemplateAplicationPermissionsQueryModel> _TEMTemplateAplicationPermissionsViewRepositoryHandler = new RepositoryHandler<TEMTemplateAplicationPermissionsBaseModel, TEMTemplateAplicationPermissionsViewModel, TEMTemplateAplicationPermissionsQueryModel>();

        private readonly ILogger<TEMFoldersHandler> _logger;
        public TEMTemplateAplicationPermissionsHandler(ILogger<TEMFoldersHandler> logger = null)
        {
            _logger = logger;
        }
      

        #region "Select Default Table"
        public async Task<Response> GetAllAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMP_APP_PERMISION.GET_ALL";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByIdAsync(decimal id)
        {
            try
            {
                var procName = "PKG_TEM_TEMP_APP_PERMISION.GET_BY_ID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByTemplateIdOrAplicationIdAsync(decimal templateId, decimal aplicationId)
        {
            try
            {
                var procName = "PKG_TEM_TEMP_APP_PERMISION.GET_BY_TEMPD_OR_APID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PTEMPLATEID", templateId, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PAPLICATIONID", aplicationId, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                return await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        #endregion

        #region "CRU Default Table"
        public async Task<Response> CreateAsync(TEMTemplateAplicationPermissionsCreateModel model, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMP_APP_PERMISION.INSERT_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PTEMPLATEID", model.Template_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PAPLICATIONID", model.Aplication_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> UpdateAsync(TEMTemplateAplicationPermissionsUpdateModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplateAplicationPermissionsViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMP_APP_PERMISION.UPDATE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PTEMPLATEID", model.Template_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PAPLICATIONID", model.Aplication_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("LASTMODIFIEDBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteAsync(decimal id, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMP_APP_PERMISION.DELETE_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                return await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel)
        {
            var listResult = new List<ResponseModel>();
            using (var unitOfWorkOracle = new UnitOfWorkOracle())
            {
                var iConn = unitOfWorkOracle.GetConnection();
                var iTrans = iConn.BeginTransaction();
                if (listId != null && listId.Count > 0)
                {
                    foreach (var id in listId)
                    {
                        var procName = "PKG_TEM_TEMP_APP_PERMISION.DELETE_ROW";
                        var dyParam = new OracleDynamicParameters();
                        dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                        dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                        dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                        var deleteResult = await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                        if (deleteResult != null)
                        {
                            listResult.Add(new ResponseModel
                            {
                                Id = deleteResult.Data.Id,
                                Name = deleteResult.Data.Name,
                                Status = deleteResult.Data.Status,
                                Message = (deleteResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                            });
                        }
                    }
                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
                else
                {
                    return new ResponseObject<List<ResponseModel>>(listResult, "Không thành công");
                }

            }

        }
        public async Task<Response> ApprovedAsync(TEMTemplateAplicationPermissionsApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplateAplicationPermissionsViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMP_APP_PERMISION.APPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }


        #endregion

        public async Task<Response> CreateListAsync(TEMTemplateAplicationPermissionsListModel model, BaseModel baseModel)
        {
            var listResult = new List<ResponseModel>();
            try
            {
                using (var unitOfWorkOracle = new UnitOfWorkOracle())
                {
                    var iConn = unitOfWorkOracle.GetConnection();
                    var iTrans = iConn.BeginTransaction();

                    var listPermission = await GetByTemplateIdOrAplicationIdAsync(model.TemplateId, 0) as ResponseObject<List<TEMTemplateAplicationPermissionsViewModel>>;
                    if (listPermission != null && listPermission.Data != null && listPermission.Data.Count() > 0)
                    {
                        if (model.Aplications!=null)
                        {
                            foreach (var item in model.Aplications)
                            {
                                var permisison = listPermission.Data.Where(s => int.Parse(s.Aplication_Id.ToString()) == int.Parse(item)).ToList().FirstOrDefault();
                                if (permisison == null)
                                {
                                    var procName = "PKG_TEM_TEMP_APP_PERMISION.INSERT_ROW";
                                    var dyParam = new OracleDynamicParameters();
                                    dyParam.Add("PTEMPLATEID", model.TemplateId, OracleMappingType.Decimal, ParameterDirection.Input);
                                    dyParam.Add("PAPLICATIONID", decimal.Parse(item), OracleMappingType.Decimal, ParameterDirection.Input);
                                    dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                                    var createResult = await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                                    if (createResult != null)
                                    {
                                        listResult.Add(new ResponseModel
                                        {
                                            Id = createResult.Data.Id,
                                            Name = createResult.Data.Name,
                                            Status = createResult.Data.Status,
                                            Message = (createResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                                        });
                                    }
                                }
                                else
                                {
                                    listPermission.Data.Where(s => int.Parse(s.Aplication_Id.ToString()) == int.Parse(item)).ToList().FirstOrDefault().IsRemove = false;
                                }
                            }
                        }
                        foreach (var item in listPermission.Data.Where(s => s.IsRemove == true))
                        {
                            var procName = "PKG_TEM_TEMP_APP_PERMISION.DELETE_ROW";
                            var dyParam = new OracleDynamicParameters();
                            dyParam.Add("PID", item.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                            dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                            dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                            await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam);
                        }
                    }
                    else
                    {
                        foreach (var id in model.Aplications)
                        {
                            var procName = "PKG_TEM_TEMP_APP_PERMISION.INSERT_ROW";
                            var dyParam = new OracleDynamicParameters();
                            dyParam.Add("PTEMPLATEID", model.TemplateId, OracleMappingType.Decimal, ParameterDirection.Input);
                            dyParam.Add("PAPLICATIONID", decimal.Parse(id), OracleMappingType.Decimal, ParameterDirection.Input);
                            dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                            dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                            var createResult = await _TEMTemplateAplicationPermissionsViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                            if (createResult != null)
                            {
                                listResult.Add(new ResponseModel
                                {
                                    Id = createResult.Data.Id,
                                    Name = createResult.Data.Name,
                                    Status = createResult.Data.Status,
                                    Message = (createResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                                });
                            }
                        }
                    }

                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
            }
            catch (Exception ex)
            {
                return new ResponseObject<List<ResponseModel>>(listResult, ex.Message);
            }
        }

    }
}
