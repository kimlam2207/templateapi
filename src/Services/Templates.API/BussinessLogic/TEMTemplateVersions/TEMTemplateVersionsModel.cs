﻿using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateVersionsBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public string Doc_Temp_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContentType { get; set; }
        public string Param_Data_Json { get; set; }
        public DateTime Effective_Start_Date { get; set; }
        public DateTime Effective_End_Date { get; set; }
        public string Description { get; set; }
        public string TemplateParentName { get; set; }
        public string StatusView { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }

    public class TEMTemplateVersionsCreateModel
    {
        public decimal Template_Id { get; set; }
        public string Doc_Temp_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContentType { get; set; }
        public string Param_Data_Json { get; set; }
        public DateTime Effective_Start_Date { get; set; }
        public DateTime Effective_End_Date { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

    }

    public class TEMTemplateVersionsUpdateModel
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public string Doc_Temp_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContentType { get; set; }
        public string Param_Data_Json { get; set; }
        public DateTime Effective_Start_Date { get; set; }
        public DateTime Effective_End_Date { get; set; }
        public string Description { get; set; }

        public string Status { get; set; }
    }
    public class TEMTemplateVersionsDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMTemplateVersionsApprovedModel
    {
        public decimal Id { get; set; }

    }
    public class TEMTemplateVersionsNoApprovedModel
    {
        public decimal Id { get; set; }

    }
    public class TEMTemplateVersionsQueryModel : PaginationRequest
    {
        public string Status { get; set; }
    }

    public class TEMTemplateVersionsViewModel : TEMTemplateVersionsBaseModel
    {
    }
    public class TEMTemplateVersionsCacheModel
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public string Doc_Temp_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContentType { get; set; }
        public string Param_Data_Json { get; set; }
        public DateTime Effective_Start_Date { get; set; }
        public DateTime Effective_End_Date { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

    }
}
