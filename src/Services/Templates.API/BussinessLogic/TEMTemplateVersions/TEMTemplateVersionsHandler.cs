﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;
using Templates.API.BussinessLogic;
using TTemplates.API.BussinessLogic;
using Microsoft.Extensions.Caching.Distributed;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateVersionsHandler : ITEMTemplateVersionsHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMTemplateVersions, TEMTemplateVersionsBaseModel, TEMTemplateVersionsQueryModel> _TEMTemplateVersionsRepositoryHandler = new RepositoryHandler<TEMTemplateVersions, TEMTemplateVersionsBaseModel, TEMTemplateVersionsQueryModel>();
        private readonly RepositoryHandler<TEMTemplateVersionsBaseModel, TEMTemplateVersionsViewModel, TEMTemplateVersionsQueryModel> _TEMTemplateVersionsViewRepositoryHandler = new RepositoryHandler<TEMTemplateVersionsBaseModel, TEMTemplateVersionsViewModel, TEMTemplateVersionsQueryModel>();
        private readonly IRedisService _redisService;
        private readonly ILogger<TEMTemplateVersionsHandler> _logger;
        public TEMTemplateVersionsHandler(ILogger<TEMTemplateVersionsHandler> logger = null, 
                                            IRedisService redisService=null
                                           )
        {
            _logger = logger;
            _redisService = redisService;
           // _templatesHandler = templatesHandler;
        }
      

        #region "Select Default Table"
        public async Task<Response> GetAllAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_VERSION.GET_ALL";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetAllActiveAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_VERSION.GET_ALL_ACTIVE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByIdAsync(decimal id)
        {
            try
            {
                var cacheKey = Helpers.GetConfig("Redis:Key") + "TEMTemplateVertions" + Convert.ToInt32(id); ;
                var cachedValue = await _redisService.GetFromCache<TEMTemplateVersionsViewModel>(cacheKey);
                if (cachedValue != null)
                {
                    ResponseObject<TEMTemplateVersionsViewModel> resultCache = new ResponseObject<TEMTemplateVersionsViewModel>(cachedValue);
                    resultCache.StatusCode = Utils.StatusCode.Success;
                    resultCache.Message = "Thành công";
                    return resultCache;
                }

                var procName = "PKG_TEM_TEMPLATE_VERSION.GET_BY_ID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> GetByTemplateIdAsync(decimal id)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_VERSION.GET_BY_TEMPLATEID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PTEMPLATEID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> GetByTemplateCodeAsync(string code)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_VERSION.GET_BY_TEMPLATECODE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PTEMPLATECODE", code, OracleMappingType.Varchar2, ParameterDirection.Input);
                return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        #endregion

        #region "CRU Default Table"
        public async Task<Response> CreateAsync(TEMTemplateVersionsCreateModel model, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_VERSION.INSERT_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PTEMPLATEID", model.Template_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PDOCTEMPID", model.Doc_Temp_Id, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PTEMPLATENAME", model.Template_Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PCONTENTYPE", model.Template_ContentType, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PPARAMDATAJSON", model.Param_Data_Json, OracleMappingType.Clob, ParameterDirection.Input);
                dyParam.Add("PEFFECTIVESTARTDATE", model.Effective_Start_Date, OracleMappingType.Date, ParameterDirection.Input);
                dyParam.Add("PEFFECTIVEENDDATE", model.Effective_End_Date, OracleMappingType.Date, ParameterDirection.Input);
                dyParam.Add("PDECRIPTION", model.Description, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> UpdateAsync(TEMTemplateVersionsUpdateModel model, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_VERSION.UPDATE_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PTEMPLATEID", model.Template_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PDOCTEMPID", model.Doc_Temp_Id, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PTEMPLATENAME", model.Template_Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PCONTENTYPE", model.Template_ContentType, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PPARAMDATAJSON", model.Param_Data_Json, OracleMappingType.Clob, ParameterDirection.Input);
                dyParam.Add("PEFFECTIVESTARTDATE", model.Effective_Start_Date, OracleMappingType.Date, ParameterDirection.Input);
                dyParam.Add("PEFFECTIVEENDDATE", model.Effective_End_Date, OracleMappingType.Date, ParameterDirection.Input);
                dyParam.Add("PDECRIPTION", model.Description, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("LASTMODIFIEDBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> DeleteAsync(decimal id, BaseModel baseModel, ITEMTemplatesHandler _templatesHandler)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_VERSION.DELETE_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                var response = await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracle(procName, dyParam) as ResponseObject<ResponseModel>;
                if (response != null && response.Data != null && response.Data.Status == "00")
                {
                    SetRedisTemplateVersion(id);
                    var version = await GetByIdAsync(id) as ResponseObject<TEMTemplateVersionsViewModel>;
                    var responselistversionbytemplateid = await GetByTemplateIdAsync(version.Data.Template_Id) as ResponseObject<List<TEMTemplateVersionsViewModel>>;
                    if (responselistversionbytemplateid.Data.Count == 1)
                    {
                        await _templatesHandler.DeleteAsync(version.Data.Template_Id, baseModel);
                    }
                }
                
                return response;
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel, ITEMTemplatesHandler _templatesHandler)
        {
            var listResult = new List<ResponseModel>();
            using (var unitOfWorkOracle = new UnitOfWorkOracle())
            {
                var iConn = unitOfWorkOracle.GetConnection();
                var iTrans = iConn.BeginTransaction();
                if (listId != null && listId.Count > 0)
                {
                    foreach (var id in listId)
                    {
                        var procName = "PKG_TEM_TEMPLATE_VERSION.DELETE_ROW";
                        var dyParam = new OracleDynamicParameters();
                        dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                        dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                        dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                        var deleteResult = await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                        if (deleteResult != null)
                        {
                            SetRedisTemplateVersion(id);
                            var version = await GetByIdAsync(id) as ResponseObject<TEMTemplateVersionsViewModel>;
                            var responseListVersionByTemplateId = await GetByTemplateIdAsync(version.Data.Template_Id) as ResponseObject<List<TEMTemplateVersionsViewModel>>;
                            if (responseListVersionByTemplateId.Data.Count == 1)
                            {
                                await _templatesHandler.DeleteAsync(version.Data.Template_Id, baseModel);
                            }
                            listResult.Add(new ResponseModel
                            {
                                Id = deleteResult.Data.Id,
                                Name = deleteResult.Data.Name,
                                Status = deleteResult.Data.Status,
                                Message = (deleteResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                            });
                        }
                    }
                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
                else
                {
                    return new ResponseObject<List<ResponseModel>>(listResult, "Không thành công");
                }

            }

        }
        public async Task<Response> ApprovedAsync(TEMTemplateVersionsApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplateVersionsViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMPLATE_VERSION.APPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> NoApprovedAsync(TEMTemplateVersionsNoApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplateVersionsViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMPLATE_VERSION.NOAPPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMTemplateVersionsViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        #endregion

        #region Redis
        public async void SetRedisTemplateVersion(decimal id)
        {
            var options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(Convert.ToInt32(Helpers.GetConfig("Redis:Expired"))));

            //set cache lên redis với bảng template

            var cacheKey = Helpers.GetConfig("Redis:Key") + "TEMTemplateVertions" + Convert.ToInt32(id);
            //remove cache cũ
            await _redisService.ClearCache(cacheKey);
            var modelCacheTemplate = await GetByIdAsync(id) as ResponseObject<TEMTemplateVersionsViewModel>;

            // cachedTime = "Expired";
            // Nạp  giá trị mới cho cache
            await _redisService.SetCache<TEMTemplateVersionsViewModel>(cacheKey, modelCacheTemplate.Data, options);
        }
        #endregion
    }
}
