﻿using System;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateVersions
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public string Doc_Temp_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContentType { get; set; }
        public string Param_Data_Json { get; set; }
        public DateTime Effective_Start_Date { get; set; }
        public DateTime Effective_End_Date { get; set; }
        public string Description { get; set; }

        // Default
        public string Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string ApproverBy { get; set; }
        
        public DateTime ApproverDate { get; set; }
    }
}
