﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMTemplateVersionsHandler
    {
        //Get
        Task<Response> GetAllAsync();
        Task<Response> GetAllActiveAsync();
        Task<Response> GetByIdAsync(decimal id);
        Task<Response> GetByTemplateIdAsync(decimal id);
        Task<Response> GetByTemplateCodeAsync(string code);
        //CRUD
        Task<Response> CreateAsync(TEMTemplateVersionsCreateModel model, BaseModel baseModel);
        Task<Response> UpdateAsync(TEMTemplateVersionsUpdateModel model, BaseModel baseModel);
        Task<Response> DeleteAsync(decimal id, BaseModel baseModel, ITEMTemplatesHandler _templatesHandler);
        Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel, ITEMTemplatesHandler _templatesHandler);
        Task<Response> ApprovedAsync(TEMTemplateVersionsApprovedModel model, BaseModel baseModel);
        Task<Response> NoApprovedAsync(TEMTemplateVersionsNoApprovedModel model, BaseModel baseModel);

        #region Redis
        void SetRedisTemplateVersion(decimal id);
        #endregion
    }
}
