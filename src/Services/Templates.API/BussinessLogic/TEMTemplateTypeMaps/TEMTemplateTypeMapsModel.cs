﻿using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateTypeMapsBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Template_Type_Id { get; set; }
        public string TemplateName { get; set; }
        public string TemplateTypeName { get; set; }

        public string StatusView { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }

    public class TEMTemplateTypeMapsCreateModel
    {
        public decimal Template_Id { get; set; }
        public decimal Template_Type_Id { get; set; }
        public string Status { get; set; }
    }

    public class TEMTemplateTypeMapsCreateListModel
    {
        public decimal Template_Id { get; set; }
        public List<string> TemplateTypes { get; set; }
    }

    public class TEMTemplateTypeMapsUpdateModel
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Template_Type_Id { get; set; }

        public string Status { get; set; }
    }
    public class TEMTemplateTypeMapsDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMTemplateTypeMapsApprovedModel
    {
        public decimal Id { get; set; }
    }
    public class TEMTemplateTypeMapsQueryModel : PaginationRequest
    {
        public string Status { get; set; }
    }

    public class TEMTemplateTypeMapsViewModel : TEMTemplateTypeMapsBaseModel
    {
    }
}
