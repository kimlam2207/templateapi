﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMTemplateTypeMapsHandler
    {
        //Get
        Task<Response> GetAllAsync();
        Task<Response> GetByIdAsync(decimal id);
        Task<Response> GetByTemplateIdAsync(decimal id);
        Task<Response> GetByTemplateIdAndTemplateTypeIdAsync(decimal templateId, decimal templateTypeId);
        //CRUD
        Task<Response> CreateAsync(TEMTemplateTypeMapsCreateModel model, BaseModel baseModel);
        Task<Response> UpdateAsync(TEMTemplateTypeMapsUpdateModel model, BaseModel baseModel);
        Task<Response> DeleteAsync(decimal id, BaseModel baseModel);
        Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel);

        Task<Response> CreateListAsync(TEMTemplateTypeMapsCreateListModel model, BaseModel baseModel);
    }
}
