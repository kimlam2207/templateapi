﻿using System;
using Utils;

namespace TTemplates.API.BussinessLogic
{
    public class TEMTemplateTypeMaps
    {
        public decimal Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Template_Type_Id { get; set; }
     
        // Default
        public string Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string ApproverBy { get; set; }
        
        public DateTime ApproverDate { get; set; }
    }
}
