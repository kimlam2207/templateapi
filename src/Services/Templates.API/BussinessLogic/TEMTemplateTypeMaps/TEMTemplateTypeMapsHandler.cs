﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Templates.API.Infrastructure.Migrations;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;
using TTemplates.API.BussinessLogic;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateTypeMapsHandler : ITEMTemplateTypeMapsHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMTemplateTypeMaps, TEMTemplateTypeMapsBaseModel, TEMTemplateTypeMapsQueryModel> _TEMTemplateTypeMapsRepositoryHandler = new RepositoryHandler<TEMTemplateTypeMaps, TEMTemplateTypeMapsBaseModel, TEMTemplateTypeMapsQueryModel>();
        private readonly RepositoryHandler<TEMTemplateTypeMapsBaseModel, TEMTemplateTypeMapsViewModel, TEMTemplateTypeMapsQueryModel> _TEMTemplateTypeMapsViewRepositoryHandler = new RepositoryHandler<TEMTemplateTypeMapsBaseModel, TEMTemplateTypeMapsViewModel, TEMTemplateTypeMapsQueryModel>();

        private readonly ILogger<TEMTemplateTypeMapsHandler> _logger;
        public TEMTemplateTypeMapsHandler(ILogger<TEMTemplateTypeMapsHandler> logger = null)
        {
            _logger = logger;
        }
      

        #region "Select Default Table"
        public async Task<Response> GetAllAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.GET_ALL";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByIdAsync(decimal id)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.GET_BY_ID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByTemplateIdAsync(decimal id)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.GET_BY_TEMPLATEID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PTEMPLATEID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByTemplateIdAndTemplateTypeIdAsync(decimal templateId,decimal templateTypeId)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.GET_BY_TEMPLATEID_TYPEID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PTEMPLATEID", templateId, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PTEMPLATETYPEID", templateTypeId, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        #endregion

        #region "CRU Default Table"
        public async Task<Response> CreateAsync(TEMTemplateTypeMapsCreateModel model, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.INSERT_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PTEMPLATEID", model.Template_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PTEMPLATETYPEID", model.Template_Type_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> UpdateAsync(TEMTemplateTypeMapsUpdateModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplateTypeMapsViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMPLATE_TYPE_MAP.UPDATE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PTEMPLATEID", model.Template_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PTEMPLATETYPEID", model.Template_Type_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("LASTMODIFIEDBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteAsync(decimal id, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.DELETE_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                return await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel)
        {
            var listResult = new List<ResponseModel>();
            using (var unitOfWorkOracle = new UnitOfWorkOracle())
            {
                var iConn = unitOfWorkOracle.GetConnection();
                var iTrans = iConn.BeginTransaction();
                if (listId != null && listId.Count > 0)
                {
                    foreach (var id in listId)
                    {
                        var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.DELETE_ROW";
                        var dyParam = new OracleDynamicParameters();
                        dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                        dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                        dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                        var deleteResult = await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                        if (deleteResult != null)
                        {
                            listResult.Add(new ResponseModel
                            {
                                Id = deleteResult.Data.Id,
                                Name = deleteResult.Data.Name,
                                Status = deleteResult.Data.Status,
                                Message = (deleteResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                            });
                        }
                    }
                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
                else
                {
                    return new ResponseObject<List<ResponseModel>>(listResult, "Không thành công");
                }

            }

        }

        public async Task<Response> CreateListAsync(TEMTemplateTypeMapsCreateListModel model, BaseModel baseModel)
        {
            var listResult = new List<ResponseModel>();
            try
            {
                using (var unitOfWorkOracle = new UnitOfWorkOracle())
                {
                    var iConn = unitOfWorkOracle.GetConnection();
                    var iTrans = iConn.BeginTransaction();

                    var responseTemplateTypeMap = await GetByTemplateIdAsync(model.Template_Id) as ResponseObject<List<TEMTemplateTypeMapsViewModel>>;
                    if (responseTemplateTypeMap != null && responseTemplateTypeMap.Data != null && responseTemplateTypeMap.Data.Count > 0)
                    {
                        foreach (var item in responseTemplateTypeMap.Data)
                        {
                            var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.DELETE_ROW";
                            var dyParam = new OracleDynamicParameters();
                            dyParam.Add("PID", item.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                            dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                            dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                            await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam);
                        }
                    }

                    //insert bảng type map
                    if (model.TemplateTypes != null && model.TemplateTypes.Count() > 0)
                    {
                        foreach (var item in model.TemplateTypes)
                        {
                            var procName = "PKG_TEM_TEMPLATE_TYPE_MAP.INSERT_ROW";
                            var dyParam = new OracleDynamicParameters();
                            dyParam.Add("PTEMPLATEID", model.Template_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                            dyParam.Add("PTEMPLATETYPEID", decimal.Parse(item), OracleMappingType.Decimal, ParameterDirection.Input);
                            dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                            dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                            var createModel= await _TEMTemplateTypeMapsViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;
                            if (createModel != null)
                            {
                                listResult.Add(new ResponseModel
                                {
                                    Id = createModel.Data.Id,
                                    Name = createModel.Data.Name,
                                    Status = createModel.Data.Status,
                                    Message = (createModel.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                                });
                            }
                        }
                    }

                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
            }
            catch (Exception ex)
            {
                return new ResponseObject<List<ResponseModel>>(listResult, ex.Message);
            }
        }

        #endregion
    }
}
