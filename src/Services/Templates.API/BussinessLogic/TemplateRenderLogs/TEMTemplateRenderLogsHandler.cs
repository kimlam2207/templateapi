﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Templates.API.Infrastructure.Migrations;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;
using TTemplates.API.BussinessLogic;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateRenderLogsHandler : ITEMTemplateRenderLogsHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMTemplateRenderLogs, TEMTemplateRenderLogsBaseModel, TEMTemplateRenderLogsQueryModel> _TEMTemplateRenderLogsRepositoryHandler = new RepositoryHandler<TEMTemplateRenderLogs, TEMTemplateRenderLogsBaseModel, TEMTemplateRenderLogsQueryModel>();
        private readonly RepositoryHandler<TEMTemplateRenderLogsBaseModel, TEMTemplateRenderLogsViewModel, TEMTemplateRenderLogsQueryModel> _TEMTemplateRenderLogsViewRepositoryHandler = new RepositoryHandler<TEMTemplateRenderLogsBaseModel, TEMTemplateRenderLogsViewModel, TEMTemplateRenderLogsQueryModel>();

        private readonly ILogger<TEMTemplateRenderLogsHandler> _logger;
        public TEMTemplateRenderLogsHandler(ILogger<TEMTemplateRenderLogsHandler> logger = null)
        {
            _logger = logger;
        }


        #region "Select Default Table"
        public async Task<Response> GetAllAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_RENDER_LOG.GET_ALL";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateRenderLogsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
     

        #endregion

        #region "CRU Default Table"
        public async Task<Response> CreateAsync(TEMTemplateRenderLogsCreateModel model, BaseModel baseModel)
        {
            try
            {               
                var procName = "PKG_TEM_TEMPLATE_RENDER_LOG.INSERT_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PDOCID", model.Doc_Id, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PTEMPLATEID", model.Template_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PAPLICATIONID", model.Aplication_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PTEMPLATETYPEMAPPINGID", model.Template_Type_Mapping_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PTEMPLATEVERSIONID", model.Template_Version_Id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PTEMPLATENAME", model.Template_Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PTEMPLATECONTENTYPE", model.Template_ContentType, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PDATAJSON", model.Data_Json, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMTemplateRenderLogsViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> GetByFilterAsync(TEMTemplateRenderLogsQueryModel filterModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_RENDER_LOG.GET_BY_FILTER";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("pPageSize", filterModel.PageSize, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pPageIndex", filterModel.PageIndex, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pFullTextSearch", filterModel.FullTextSearch, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pTemplateId", filterModel.TemplateId, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pAplicationId", filterModel.AplicationId, OracleMappingType.Varchar2, ParameterDirection.Input);
                return await _TEMTemplateRenderLogsViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        #endregion
    }
}
