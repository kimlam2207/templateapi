﻿using System;
using Utils;

namespace TTemplates.API.BussinessLogic
{
    public class TEMTemplateRenderLogs
    {
        public decimal Id { get; set; }
        public string Doc_Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Aplication_Id { get; set; }
        public decimal Template_Type_Mapping_Id { get; set; }
        public decimal Template_Version_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContentType { get; set; }
        public string Data_Json { get; set; }

        // Default
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string ApproverBy { get; set; }
        
        public DateTime ApproverDate { get; set; }
    }
}
