﻿using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateRenderLogsBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public string Doc_Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Aplication_Id { get; set; }
        public decimal Template_Type_Mapping_Id { get; set; }
        public decimal Template_Version_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContenType { get; set; }
        public string Data_Json { get; set; }
        public string TemplateParentName { get; set; }
        public string TemplateVersionName { get; set; }
        public string AplicationName { get; set; }
        public string StatusView { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }

    public class TEMTemplateRenderLogsCreateModel
    {
        public string Doc_Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Aplication_Id { get; set; }
        public decimal Template_Type_Mapping_Id { get; set; }
        public decimal Template_Version_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContentType { get; set; }
        public string Data_Json { get; set; }
    }

    public class TEMTemplateRenderLogsUpdateModel
    {
        public decimal Id { get; set; }
        public string Doc_Id { get; set; }
        public decimal Template_Id { get; set; }
        public decimal Aplication_Id { get; set; }
        public decimal Template_Type_Mapping_Id { get; set; }
        public decimal Template_Version_Id { get; set; }
        public string Template_Name { get; set; }
        public string Template_ContentType { get; set; }
        public string Data_Json { get; set; }
    }
    public class TEMTemplateRenderLogsDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMTemplateRenderLogsApprovedModel
    {
        public decimal Id { get; set; }
    }
    public class TEMTemplateRenderLogsQueryModel : PaginationRequest
    {
        public decimal? TemplateId { get; set; }
        public decimal? AplicationId { get; set; }

    }

    public class TEMTemplateRenderLogsViewModel : TEMTemplateRenderLogsBaseModel
    {
    }
}
