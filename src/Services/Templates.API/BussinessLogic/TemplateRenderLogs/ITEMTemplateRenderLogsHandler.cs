﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMTemplateRenderLogsHandler
    {
        //Get
        Task<Response> GetAllAsync();

        Task<Response> GetByFilterAsync(TEMTemplateRenderLogsQueryModel filterModel);

        //CRUD
        Task<Response> CreateAsync(TEMTemplateRenderLogsCreateModel model, BaseModel baseModel);


    }
}
