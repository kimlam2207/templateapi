﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Templates.API.Infrastructure.Migrations;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;
using TTemplates.API.BussinessLogic;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateTypesHandler : ITEMTemplatesTypeHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMTemplateTypes, TEMTemplateTypesBaseModel, TEMTemplateTypesQueryModel> _TEMTemplateTypesRepositoryHandler = new RepositoryHandler<TEMTemplateTypes, TEMTemplateTypesBaseModel, TEMTemplateTypesQueryModel>();
        private readonly RepositoryHandler<TEMTemplateTypesBaseModel, TEMTemplateTypesViewModel, TEMTemplateTypesQueryModel> _TEMemplateTypesViewRepositoryHandler = new RepositoryHandler<TEMTemplateTypesBaseModel, TEMTemplateTypesViewModel, TEMTemplateTypesQueryModel>();

        private readonly ILogger<TEMTemplateTypesHandler> _logger;
        public TEMTemplateTypesHandler(ILogger<TEMTemplateTypesHandler> logger = null)
        {
            _logger = logger;
        }
      

        #region "Select Default Table"
        public async Task<Response> GetAllAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE.GET_ALL";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> GetAllActiveAsync()
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE.GET_ALL_ACTIVE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByListIdAsync(string listId)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE.GET_BY_LIST_ID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PLISTID", listId, OracleMappingType.NVarchar2, ParameterDirection.Output);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByIdAsync(decimal id)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE.GET_BY_ID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetByCodeAsync(decimal id, string name)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE.GET_BY_CODE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PCODE", name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        public async Task<Response> GetByFilterAsync(TEMTemplateTypesQueryModel filterModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE.GET_BY_FILTER";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("pPageSize", filterModel.PageSize, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pPageIndex", filterModel.PageIndex, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pFullTextSearch", filterModel.FullTextSearch, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pName", (string.IsNullOrEmpty(filterModel.Name)) ? null : filterModel.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pStatus", (string.IsNullOrEmpty(filterModel.Status)) ? null : filterModel.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                return await _TEMTemplateTypesRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }

        #endregion

        #region "CRU Default Table"
        public async Task<Response> CreateAsync(TEMTemplateTypesCreateModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByCodeAsync(0,model.Code) as ResponseObject<TEMTemplateTypesViewModel>;
                if (objQuery != null  && objQuery.Data !=null)
                {
                    return new ResponseError(StatusCode.Fail, "Mã code đã tồn tại");
                }
                var procName = "PKG_TEM_TEMPLATE_TYPE.INSERT_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PCODE ", model.Code, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PNAME ", model.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> UpdateAsync(TEMTemplateTypesUpdateModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplateTypesViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    objQuery = await GetByCodeAsync(model.Id, model.Code) as ResponseObject<TEMTemplateTypesViewModel>;
                    if (objQuery != null && objQuery.Data != null)
                    {
                        return new ResponseError(StatusCode.Fail, "Mã code đã tồn tại");
                    }

                    var procNam = "PKG_TEM_TEMPLATE_TYPE.UPDATE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PCODE ", model.Code, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PNAME ", model.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("LASTMODIFIEDBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteAsync(decimal id, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_TEMPLATE_TYPE.DELETE_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel)
        {
            var listResult = new List<ResponseModel>();
            using (var unitOfWorkOracle = new UnitOfWorkOracle())
            {
                var iConn = unitOfWorkOracle.GetConnection();
                var iTrans = iConn.BeginTransaction();
                if (listId != null && listId.Count > 0)
                {
                    foreach (var id in listId)
                    {
                        var procName = "PKG_TEM_TEMPLATE_TYPE.DELETE_ROW";
                        var dyParam = new OracleDynamicParameters();
                        dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                        dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                        dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                        var deleteResult = await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                        if (deleteResult != null)
                        {
                            listResult.Add(new ResponseModel
                            {
                                Id = deleteResult.Data.Id,
                                Name = deleteResult.Data.Name,
                                Status = deleteResult.Data.Status,
                                Message = (deleteResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                            });
                        }
                    }
                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
                else
                {
                    return new ResponseObject<List<ResponseModel>>(listResult, "Không thành công");
                }

            }

        }
        public async Task<Response> ApprovedAsync(TEMTemplateTypesApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplateTypesViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMPLATE_TYPE.APPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> NoApprovedAsync(TEMTemplateTypesApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetByIdAsync(model.Id) as ResponseObject<TEMTemplateTypesViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_TEMPLATE_TYPE.NOAPPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMemplateTypesViewRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        #endregion
    }
}
