﻿using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMTemplateTypesBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string StatusView { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }

    public class TEMTemplateTypesCreateModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }

    public class TEMTemplateTypesUpdateModel
    {
        public decimal Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
    public class TEMTemplateTypesDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMTemplateTypesApprovedModel
    {
        public decimal Id { get; set; }
    }
    public class TEMTemplateTypesQueryModel : PaginationRequest
    {
        public string Status { get; set; }
        public string Name { get; set; }
    }

    public class TEMTemplateTypesViewModel : TEMTemplateTypesBaseModel
    {
    }
}
