﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMTemplatesTypeHandler
    {
        //Get
        Task<Response> GetAllAsync();
        Task<Response> GetAllActiveAsync();
        Task<Response> GetByListIdAsync(string listId);
        Task<Response> GetByIdAsync(decimal id);
        Task<Response> GetByCodeAsync(decimal id, string code);

        Task<Response> GetByFilterAsync(TEMTemplateTypesQueryModel filterModel);

        //CRUD
        Task<Response> CreateAsync(TEMTemplateTypesCreateModel model, BaseModel baseModel);
        Task<Response> UpdateAsync(TEMTemplateTypesUpdateModel model, BaseModel baseModel);
        Task<Response> DeleteAsync(decimal id, BaseModel baseModel);
        Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel);
        Task<Response> ApprovedAsync(TEMTemplateTypesApprovedModel model, BaseModel baseModel);
        Task<Response> NoApprovedAsync(TEMTemplateTypesApprovedModel model, BaseModel baseModel);
    }
}
