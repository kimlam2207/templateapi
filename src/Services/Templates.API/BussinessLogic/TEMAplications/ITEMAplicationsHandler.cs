﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMAplicationsHandler
    {
        //Get
        Task<Response> GetAllAsync();
        Task<Response> GetAllActiveAsync();
        Task<Response> GetByIdAsync(decimal id);
        Task<Response> GetByCodeAsync(decimal id, string code);
        Task<Response> GetByFilterAsync(TEMAplicationsQueryModel filterModel);

        //CRUD
        Task<Response> CreateAsync(TEMAplicationsCreateModel model, BaseModel baseModel);
        Task<Response> UpdateAsync(TEMAplicationsUpdateModel model, BaseModel baseModel);
        Task<Response> DeleteAsync(decimal id, BaseModel baseModel);
        Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel);
        Task<Response> ApprovedAsync(TEMAplicationsApprovedModel model, BaseModel baseModel);
        Task<Response> NoApprovedAsync(TEMAplicationsApprovedModel model, BaseModel baseModel);
    }
}
