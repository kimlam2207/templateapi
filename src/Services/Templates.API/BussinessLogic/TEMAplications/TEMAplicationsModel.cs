﻿using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMAplicationsBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }

        public string StatusView { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }

    public class TEMAplicationsCreateModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }

        public string Status { get; set; }
    }

    public class TEMAplicationsUpdateModel
    {
        public decimal Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }

        public string Status { get; set; }
    }
    public class TEMAplicationsDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMAplicationsApprovedModel
    {
        public decimal Id { get; set; }
    }
    public class TEMAplicationsQueryModel : PaginationRequest
    {
        public string Name { get; set; }
        public string Status { get; set; }
    }

    public class TEMAplicationsViewModel : TEMAplicationsBaseModel
    {
    }
}
