﻿using System;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMAplications
    {
        public decimal Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        // Default
        public string Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string ApproverBy { get; set; }
        
        public DateTime ApproverDate { get; set; }
    }
}
