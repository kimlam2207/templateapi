﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Templates.API.Infrastructure.Migrations;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using DocumentFormat.OpenXml.EMMA;
using Templates.API.Velocity;

namespace Templates.API.BussinessLogic
{
    public class ECMHandler : IECMHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMTemplates, TEMTemplatesBaseModel, TEMTemplatesQueryModel> _TEMTemplatesRepositoryHandler = new RepositoryHandler<TEMTemplates, TEMTemplatesBaseModel, TEMTemplatesQueryModel>();
        private readonly RepositoryHandler<TEMTemplatesBaseModel, TEMTemplatesViewModel, TEMTemplatesQueryModel> _TEMTemplatesViewRepositoryHandler = new RepositoryHandler<TEMTemplatesBaseModel, TEMTemplatesViewModel, TEMTemplatesQueryModel>();
        //private readonly TEMTemplateVersionsHandler _TEMTemplateVersionsInterfaceHandler = new TEMTemplateVersionsHandler();
        private readonly ITEMTemplateVersionsHandler _TEMTemplateVersionsInterfaceHandler;
        private readonly ILogger<TEMFoldersHandler> _logger;
        private readonly IRedisService _redisService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ITEMTemplateTypeMapsHandler _TEMTemplateTypeMapsInterfaceHandler;
        private readonly ITEMTemplateAplicationPermissionsHandler _TEMTemplateAplicationPermissionsHandler;
        private readonly ITEMAplicationsHandler _TEMAplicationsHandler;
        private readonly ITEMTemplatesTypeHandler _TEMTemplatesTypeHandler;
        private readonly ITEMTemplatesHandler _TEMTemplatesHandler;
        private readonly ITEMTemplateRenderLogsHandler _TEMTemplateRenderLogsHandler;
        private readonly string _contentRootPath;

        public ECMHandler(ILogger<TEMFoldersHandler> logger = null, 
                                    ITEMTemplateVersionsHandler templateVersionsInterfaceHandler=null,
                                    IRedisService redisService=null,
                                    IHostingEnvironment hostingEnvironment=null,
                                    ITEMTemplateTypeMapsHandler templateTypeMapsInterfaceHandler=null,
                                    ITEMTemplateAplicationPermissionsHandler templateAplicationPermissionsHandler=null,
                                    ITEMAplicationsHandler aplicationsHandler=null,
                                    ITEMTemplatesTypeHandler templatesTypeHandler=null,
                                    ITEMTemplatesHandler templatesHandler = null,
                                    ITEMTemplateRenderLogsHandler templateRenderLogsHandler=null)
        {
            _logger = logger;
            _TEMTemplateVersionsInterfaceHandler = templateVersionsInterfaceHandler;
            _redisService = redisService;
            _hostingEnvironment = hostingEnvironment;
            _contentRootPath = hostingEnvironment.ContentRootPath;
            _TEMTemplateTypeMapsInterfaceHandler = templateTypeMapsInterfaceHandler;
            _TEMTemplateAplicationPermissionsHandler = templateAplicationPermissionsHandler;
            _TEMAplicationsHandler = aplicationsHandler;
            _TEMTemplatesTypeHandler = templatesTypeHandler;
            _TEMTemplatesHandler = templatesHandler;
            _TEMTemplateRenderLogsHandler = templateRenderLogsHandler;
        }
      

        #region download file ECM
        public async Task<ResponseECM> DownloadFileAsync(string docId)
        {
            ResponseECM responseUploadECM = await ECM.DownloadFileFromECM(docId, _contentRootPath);
            return responseUploadECM;
        }

        public async Task<ResponseECM> RenderFileAsync(TEMTemplatesReplaceDataModel model, BaseModel baseModel)
        {
            ResponseECM response = new ResponseECM();
            try
            {
                response =  await ValidateRenderFileAsync(model);
                if (response.StatusCode==StatusCode.Fail)
                {
                    return response;
                }

                var templateVersion = await _TEMTemplateVersionsInterfaceHandler.GetByIdAsync(model.TemplateVersionId) as ResponseObject<TEMTemplateVersionsViewModel>;
                //download file từ ECM
                response = await ECM.DownloadFileFromECM(templateVersion.Data.Doc_Temp_Id, _contentRootPath);
                if (response.StatusCode == StatusCode.Fail)
                {
                    return response;
                }
                bool check = false;
                ResponseFile<MemoryStream> responeFile = new ResponseFile<MemoryStream>();

                List<string> listDoc = new List<string>() { ".docx", ".docm", ".doc", ".dox", ".dotm", ".dot" };
                List<string> listHtmlOrTex = new List<string>() { ".html", ".txt"};
                List<string> listExcel = new List<string>() { ".xls", ".xlsx" };
                string ext = Path.GetExtension(templateVersion.Data.Template_Name);
                //file template is word
                var doc = listDoc.Where(s => s.Contains(ext));
                if (doc!=null && doc.FirstOrDefault()!=null)
                {
                    responeFile = BookMark.ReplaceBookMarkWord(model.DataJson, response.properties[0][0].docContent, response.properties[0][0].docName);
                    if (responeFile.StatusCode==StatusCode.Fail)
                    {
                        response.StatusCode = StatusCode.Fail;
                        response.Message = responeFile.Message;
                    }
                    else
                    {                     
                        response.StatusCode = StatusCode.Success;
                        response.ResponseFile = responeFile;
                        response.Code = "WORD";
                        check = true;
                    }
                }
                //file template is html or txt
                var htmlortext = listHtmlOrTex.Where(s => s.Contains(ext));
                if (htmlortext!=null && htmlortext.FirstOrDefault() != null)
                {
                    byte[] bytes = Convert.FromBase64String(response.properties[0][0].docContent);
                    var templateContent = string.Empty;
                    MemoryStream memoryStream = new MemoryStream(bytes);
                    Spire.Doc.Document spire = new Spire.Doc.Document();
                    spire.LoadFromStream(memoryStream, Spire.Doc.FileFormat.Txt);
                    templateContent = spire.Document.GetText();
                    var result = await Velocity.Velocity.RenderTemplate(templateContent, model.DataJson) as ResponseObject<ResponseVelocity>;
                    if (result.StatusCode == StatusCode.Fail)
                    {
                        response.StatusCode = StatusCode.Fail;
                        response.Message = responeFile.Message;
                    }
                    else
                    {

                        if (ext.ToUpper() == ".TXT")
                        {
                            memoryStream = new MemoryStream();
                            Spire.Doc.Section s = spire.Sections[0].Clone();
                            Spire.Doc.Documents.Paragraph p = s.Paragraphs[0];
                            spire.Sections.RemoveAt(0);
                            Spire.Doc.Section sNew = spire.AddSection();
                            Spire.Doc.Documents.Paragraph NewPara = (Spire.Doc.Documents.Paragraph)p.Clone();
                            NewPara.Text = result.Data.Content;
                            sNew.Paragraphs.Add(NewPara);
                            spire.SaveToFile(memoryStream, Spire.Doc.FileFormat.Txt);
                            response.Code = "TXT";
                            check = true;
                        }
                        else
                        {
                            memoryStream = new MemoryStream();
                            Spire.Doc.Document spireHtml = new Spire.Doc.Document();
                            MemoryStream memoryStreamHtml = new MemoryStream(bytes);
                            spireHtml.LoadFromStream(memoryStreamHtml, Spire.Doc.FileFormat.Html);
                            Spire.Doc.Section s = spireHtml.Sections[0].Clone();
                            Spire.Doc.Documents.Paragraph p = s.Paragraphs[0];
                            spireHtml.Sections.RemoveAt(0);

                            Spire.Doc.Section sNew = spire.AddSection();
                            Spire.Doc.Documents.Paragraph NewPara = sNew.AddParagraph();
                            NewPara.Text = result.Data.Content;
                            NewPara.AppendHTML(result.Data.Content);
                            sNew.Paragraphs.Add(NewPara);
                            spireHtml.SaveToFile(memoryStream, Spire.Doc.FileFormat.Html);
                            response.Code = "HTML";
                            check = true;
                        }
                        memoryStream.Position = 0; //let's rewind it
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        response.StatusCode = StatusCode.Success;                      
                        response.ResponseFile = new ResponseFile<MemoryStream>(memoryStream, response.properties[0][0].docName, response.properties[0][0].fileType);                       
                    }             

                }

                //file template is excel
                var excel = listExcel.Where(s => s.Contains(ext));
                if (excel != null && excel.FirstOrDefault() != null)
                {
                    responeFile = Excel.Excel.ReplaceDataExcel(model.DataJson, response.properties[0][0].docContent, response.properties[0][0].docName);
                    if (responeFile.StatusCode == StatusCode.Fail)
                    {
                        response.StatusCode = StatusCode.Fail;
                        response.Message = responeFile.Message;
                    }
                    else
                    {
                        response.StatusCode = StatusCode.Success;
                        response.ResponseFile = responeFile;
                        response.Code = "XLSX";
                        check = true;
                    }
                }
                if (!check) //Có thể kiểm tra thêm các loại khác
                {
                    response.StatusCode = StatusCode.Fail;
                    return response;
                }

                MemoryStream memory = new MemoryStream();
                if (response != null && response.StatusCode == Utils.StatusCode.Success)
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(response.properties[0][0].docName);
                        var templateType = await _TEMTemplatesTypeHandler.GetByIdAsync(model.TemplateTypeId) as ResponseObject<TEMTemplateTypesViewModel>;
                        var templateTypeMap = await _TEMTemplateTypeMapsInterfaceHandler.GetByTemplateIdAndTemplateTypeIdAsync(model.TemplateId, model.TemplateTypeId) as ResponseObject<TEMTemplateTypeMapsViewModel>;

                        TEMTemplateRenderLogsCreateModel logModel = new TEMTemplateRenderLogsCreateModel()
                        {
                            Template_Id = model.TemplateId,
                            Aplication_Id = model.AplicationId,
                            Template_Type_Mapping_Id = templateTypeMap.Data.Id,
                            Template_Version_Id = templateVersion.Data.Id,
                            Data_Json = model.DataJson
                        };

                        Spire.Doc.Document spire = new Spire.Doc.Document();
                        switch (response.Code)
                        { 
                            //File có định dạng là word
                            case "WORD":                             
                                spire.LoadFromStream(response.ResponseFile.Data, Spire.Doc.FileFormat.Docx);
                                switch (templateType.Data.Code)
                                {
                                    case "WORD":
                                        spire.SaveToFile(memory, Spire.Doc.FileFormat.Docx);
                                        memory.Position = 0;
                                        memory.Seek(0, SeekOrigin.Begin);
                                        logModel.Template_Name = fileName + ".docx";
                                        logModel.Template_ContentType = "application/msword";
                                        var resultUploadWord = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(memory.ToArray()), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadWord;
                                        logModel.Doc_Id = resultUploadWord.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;
                                    case "PDF":
                                        spire.SaveToFile(memory, Spire.Doc.FileFormat.PDF);
                                        memory.Position = 0;
                                        memory.Seek(0, SeekOrigin.Begin);
                                        logModel.Template_Name = fileName + ".pdf";
                                        logModel.Template_ContentType = "application/pdf";
                                        var resultUploadPdf = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(memory.ToArray()), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadPdf;
                                        logModel.Doc_Id = resultUploadPdf.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;
                                    case "XLSX":
                                        spire.SaveToFile(memory, Spire.Doc.FileFormat.PDF);
                                        SautinSoft.PdfFocus pdf = new SautinSoft.PdfFocus();
                                        memory.Position = 0;
                                        memory.Seek(0, SeekOrigin.Begin);
                                        pdf.OpenPdf(memory);
                                        byte[] bytePdf=pdf.ToExcel();
                                        //memory = new MemoryStream(bytePdf);
                                        logModel.Template_Name = fileName + ".xls";
                                        logModel.Template_ContentType = "application/vnd.ms-excel";
                                        var resultUploadExcel = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(bytePdf), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadExcel;
                                        logModel.Doc_Id = resultUploadExcel.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;
                                    case "HTML":
                                        spire.SaveToFile(memory, Spire.Doc.FileFormat.Html);
                                        memory.Position = 0;
                                        memory.Seek(0, SeekOrigin.Begin);
                                        logModel.Template_Name = fileName + ".html";
                                        logModel.Template_ContentType = "text/html";
                                        var resultUploadHtml = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(memory.ToArray()), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadHtml;
                                        logModel.Doc_Id = resultUploadHtml.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;                                
                                    default:
                                        response.StatusCode = StatusCode.Fail;
                                        response.Message = "Không thể convert từ word sang " + templateType.Data.Code.ToLower();
                                        break;
                                }
                                break;

                            //File có định dạng là txt
                            case "TXT":
                                spire.LoadFromStream(response.ResponseFile.Data, Spire.Doc.FileFormat.Txt);
                                switch (templateType.Data.Code.ToUpper())
                                {
                                    case "TXT":
                                        spire.SaveToFile(memory, Spire.Doc.FileFormat.Txt);
                                        memory.Position = 0;
                                        memory.Seek(0, SeekOrigin.Begin);
                                        logModel.Template_Name = fileName + ".txt";
                                        logModel.Template_ContentType = "text/plain";
                                        var resultUploadTxt= await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(memory.ToArray()), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadTxt;
                                        logModel.Doc_Id = resultUploadTxt.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;
                                    default:
                                        response.StatusCode = StatusCode.Fail;
                                        response.Message = "txt chỉ được convert về txt";
                                        break;
                                }
                                break;

                            //File có định dạng là html
                            case "HTML":
                                spire.LoadFromStream(response.ResponseFile.Data, Spire.Doc.FileFormat.Html);
                                switch (templateType.Data.Code.ToUpper())
                                {
                                    case "WORD":
                                        spire.SaveToFile(memory, Spire.Doc.FileFormat.Docx);
                                        memory.Position = 0;
                                        memory.Seek(0, SeekOrigin.Begin);
                                        logModel.Template_Name = fileName + ".docx";
                                        logModel.Template_ContentType = "application/msword";
                                        var resultUploadWord = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(memory.ToArray()), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadWord;
                                        logModel.Doc_Id = resultUploadWord.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;
                                    case "PDF":
                                        spire.SaveToFile(memory, Spire.Doc.FileFormat.PDF);
                                        memory.Position = 0;
                                        memory.Seek(0, SeekOrigin.Begin);
                                        logModel.Template_Name = fileName + ".pdf";
                                        logModel.Template_ContentType = "application/pdf";
                                        var resultUploadPdf = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(memory.ToArray()), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadPdf;
                                        logModel.Doc_Id = resultUploadPdf.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;
                                    case "HTML":
                                        logModel.Template_Name = fileName + ".html";
                                        logModel.Template_ContentType = "text/html";
                                        var resultUploadHtml = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(response.ResponseFile.Data.ToArray()), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadHtml;
                                        logModel.Doc_Id = resultUploadHtml.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;
                                    default:
                                        response.StatusCode = StatusCode.Fail;
                                        response.Message = "Không thể convert từ word sang " + templateType.Data.Code.ToLower();
                                        break;
                                }
                                break;

                            //File có định dạng là excel
                            case "XLSX":
                                switch (templateType.Data.Code.ToUpper())
                                {
                                    case "XLSX":
                                        logModel.Template_Name = response.properties[0][0].docName;
                                        logModel.Template_ContentType = "application/vnd.ms-excel";
                                        var resultUploadExcel= await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(response.ResponseFile.Data.ToArray()), response.properties[0][0].docName, response.properties[0][0].fileType, _hostingEnvironment.ContentRootPath);
                                        response = resultUploadExcel;
                                        logModel.Doc_Id = resultUploadExcel.properties[0][0].id;
                                        await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                        break;
                                    //case "PDF":
                                    //    SautinSoft.UseOffice converToPdf = new SautinSoft.UseOffice();
                                    //    byte[] pdf = converToPdf.ConvertBytes(response.ResponseFile.Data.ToArray(),SautinSoft.UseOffice.eDirection.XLSX_to_PDF);
                                    //    logModel.Template_Name = fileName + ".pdf";
                                    //    logModel.Template_ContentType = "application/pdf";
                                    //    var resultUploadPdf = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(pdf), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                    //    response = resultUploadPdf;
                                    //    logModel.Doc_Id = resultUploadPdf.properties[0][0].id;
                                    //    await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                    //    break;
                                    //case "WORD":
                                    //    SautinSoft.UseOffice converToPdfN = new SautinSoft.UseOffice();
                                    //    byte[] pdfN = converToPdfN.ConvertBytes(response.ResponseFile.Data.ToArray(), SautinSoft.UseOffice.eDirection.XLSX_to_PDF);
                                    //    SautinSoft.PdfFocus pdfToWord = new SautinSoft.PdfFocus();
                                    //    memory = new MemoryStream(pdfN);
                                    //    memory.Position = 0; //let's rewind it
                                    //    memory.Seek(0, SeekOrigin.Begin);
                                    //    pdfToWord.ToWord(memory);

                                    //    logModel.Template_Name = fileName + ".docx";
                                    //    logModel.Template_ContentType = "application/msword";
                                    //    var resultUploadWord = await ECM.UploadFileByBase64ToECM(Convert.ToBase64String(memory.ToArray()), logModel.Template_Name, logModel.Template_ContentType, _hostingEnvironment.ContentRootPath);
                                    //    response = resultUploadWord;
                                    //    logModel.Doc_Id = resultUploadWord.properties[0][0].id;
                                    //    await _TEMTemplateRenderLogsHandler.CreateAsync(logModel, baseModel);
                                    //    break;
                                    default:
                                        response.StatusCode = StatusCode.Fail;
                                        response.Message = "Không thể convert từ word sang " + templateType.Data.Code.ToLower();
                                        break;
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        response.StatusCode = Utils.StatusCode.Fail;
                        response.Message = ex.Message;
                    }
                }
                else
                {
                    response.StatusCode = Utils.StatusCode.Fail;
                    response.Message = response.Message;
                }

                return response;

            }
            catch (Exception ex)
            {
                response.StatusCode = StatusCode.Fail;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ResponseECM> ValidateRenderFileAsync(TEMTemplatesReplaceDataModel model)
        {
            ResponseECM response = new ResponseECM();
            try
            {
                //kiểm template
                var template = await _TEMTemplatesHandler.GetByIdAsync(model.TemplateId) as ResponseObject<TEMTemplatesViewModel>;
                if (template == null || template.Data == null)
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Template không tồn tại";
                }
                else if (template != null && template.Data != null && template.Data.Status == "BLOCKED")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Template đã bị xóa";
                }
                else if (template != null && template.Data != null && template.Data.Status == "DEACTIVE")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Template chưa được duyệt";
                }
                //kiểm tra version template
                var templateVersion = await _TEMTemplateVersionsInterfaceHandler.GetByIdAsync(model.TemplateVersionId) as ResponseObject<TEMTemplateVersionsViewModel>;
                if (templateVersion == null || templateVersion.Data == null)
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Version không tồn tại";
                }
                else if (templateVersion != null && templateVersion.Data != null && templateVersion.Data.Status == "BLOCKED")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Version đã bị xóa";
                }
                else if (templateVersion != null && templateVersion.Data != null && templateVersion.Data.Status == "DEACTIVE")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Version chưa được duyệt";
                }
                else if (templateVersion != null && templateVersion.Data != null && templateVersion.Data.Effective_End_Date.Date < DateTime.Now.Date)
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Version đã hết thời gian sử dụng ";
                }
                //kiểm tra ứng dụng
                var aplication = await _TEMAplicationsHandler.GetByIdAsync(model.AplicationId) as ResponseObject<TEMAplicationsViewModel>;
                if (aplication == null || aplication.Data == null)
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Ứng dụng không tồn tại";
                }
                else if (aplication != null && aplication.Data != null && aplication.Data.Status == "BLOCKED")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Ứng dụng đã bị xóa";
                }
                else if (aplication != null && aplication.Data != null && aplication.Data.Status == "DEACTIVE")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Ứng dụng chưa được duyệt";
                }
                else if (model.Token != aplication.Data.Token)
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Mã token không hợp lệ";
                }

                //kiểm tra permission
                var permission = await _TEMTemplateAplicationPermissionsHandler.GetByTemplateIdOrAplicationIdAsync(model.TemplateId, aplication.Data.Id) as ResponseObject<List<TEMTemplateAplicationPermissionsViewModel>>;
                if (permission == null || permission.Data == null || permission.Data.Count() == 0)
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Ứng dụng không tồn tại";
                }
                else if (permission != null && permission.Data != null && permission.Data[0].Status == "BLOCKED")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Ứng dụng không có quyền sử dụng template";
                }

                //kiểm tra template type
                var templateType = await _TEMTemplatesTypeHandler.GetByIdAsync(model.TemplateTypeId) as ResponseObject<TEMTemplateTypesViewModel>;
                if (templateType == null || templateType.Data == null)
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Loại xuất không tồn tại";
                }
                else if (templateType != null && templateType.Data != null && templateType.Data.Status == "BLOCKED")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Loại xuất đã bị khóa";
                }
                else if (templateType != null && templateType.Data != null && templateType.Data.Status == "DEACTIVE")
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Loại xuất chưa được duyệt";
                }
                //kiểm tra template type map
                var templateTypeMap = await _TEMTemplateTypeMapsInterfaceHandler.GetByTemplateIdAndTemplateTypeIdAsync(model.TemplateId,model.TemplateTypeId) as ResponseObject<TEMTemplateTypeMapsViewModel>;
                if (templateTypeMap == null || templateTypeMap.Data == null)
                {
                    response.StatusCode = StatusCode.Fail;
                    response.Message = "Template không được chuyển đổi sang loại xuất này";
                }

            }
            catch (Exception ex)
            {
                response.StatusCode = StatusCode.Fail;
                response.Message = ex.Message;
            }
            return response;
        }

        #endregion


    }
}
