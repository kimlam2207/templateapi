﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface IECMHandler
    {
        //Get

        Task<ResponseECM> RenderFileAsync(TEMTemplatesReplaceDataModel model, BaseModel baseModel);

        Task<ResponseECM> DownloadFileAsync(string docId);
    }
}
