﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utils;

namespace Templates.API.BussinessLogic
{
    public interface ITEMFoldersHandler
    {
        // 
        Task<Response> GetAllActiveTreAsync();
        // 
        Task<Response> GetByFilterAsync(TEMFoldersQueryModel model);
        Task<Response> GetAllActiveAsync(string type = "");
        Task<Response> GetAllAsync(string Type);
        Task<Response> GetListByUserPermisisonAsync(decimal userId);
        Task<Response> GetById(decimal id);
        // 
        Task<Response> CreateAsync(TEMFoldersCreateModel model, BaseModel baseModel);
        Task<Response> UpdateAsync( TEMFoldersUpdateModel model, BaseModel baseModel);
        Task<Response> DeleteAsync(decimal id, BaseModel baseModel);
        Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel);
        Task<Response> ApprovedAsync(TEMFoldersApprovedModel model, BaseModel baseModel);
        Task<Response> NoApprovedAsync(TEMFoldersApprovedModel model, BaseModel baseModel);
        Task<Response> CheckCodeAsync(decimal id,string code);
    }
}
