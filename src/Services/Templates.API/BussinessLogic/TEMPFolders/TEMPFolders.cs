﻿using System;
using Utils;

namespace Templates.API.Infrastructure.Migrations
{
    public class TEMFolders
    {
        public decimal Id { get; set; }
        public decimal Parent_Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        // Default
        public string Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string ApproverBy { get; set; }
        public DateTime ApproverDate { get; set; }
    }
}
