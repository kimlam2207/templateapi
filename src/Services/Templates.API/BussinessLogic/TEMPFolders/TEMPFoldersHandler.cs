﻿using System.Threading.Tasks;
using Utils;
using API.Infrastructure.Repositories;
using Templates.API.Infrastructure.Migrations;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.BuildingBlocks.EventBus.Helpers;

namespace Templates.API.BussinessLogic
{
    public class TEMFoldersHandler : ITEMFoldersHandler
    {
        private string strWebURL = Helpers.GetConfig("Web:BackEnd");
        private readonly RepositoryHandler<TEMFolders, TEMFoldersViewModel, TEMFoldersQueryModel> _TEMFoldersRepositoryHandler = new RepositoryHandler<TEMFolders, TEMFoldersViewModel, TEMFoldersQueryModel>();
        private readonly RepositoryHandler<TEMFoldersBaseModel, TEMFoldersViewModel, TEMFoldersQueryModel> _TEMFoldersVViewRepositoryHandler = new RepositoryHandler<TEMFoldersBaseModel, TEMFoldersViewModel, TEMFoldersQueryModel>();

        private readonly ILogger<TEMFoldersHandler> _logger;
        public TEMFoldersHandler(ILogger<TEMFoldersHandler> logger = null)
        {
            _logger = logger;
        }

        #region "Other for Table"
        public async Task<Response> GetAllActiveTreAsync()
        {
            try
            {
                List<TEMFoldersViewModel> ojbListResponse = new List<TEMFoldersViewModel>();
                var itm = await GetAllActiveAsync() as ResponseObject<List<TEMFoldersViewModel>>;
                if (itm != null && itm.Data != null && itm.Data.Count > 0)
                {
                    List<TEMFoldersViewModel> ojbListAll = itm.Data;
                    var ojbListParent = ojbListAll.Where(x => x.Parent_Id == 0).OrderBy(x => x.Code);
                    foreach (TEMFoldersViewModel itemRun in ojbListParent)
                    {
                        TEMFoldersViewModel item = itemRun;
                        item.itemSub = GetAllActiveTreSubAsync(ojbListAll, item);
                        ojbListResponse.Add(item);
                    }
                    return new ResponseObject<List<TEMFoldersViewModel>>(ojbListResponse, "Thành công", StatusCode.Success);
                }
                else
                {
                    return new ResponseObject<List<TEMFoldersViewModel>>(null, "Không có dữ liệu", StatusCode.Success);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public List<TEMFoldersViewModel> GetAllActiveTreSubAsync(List<TEMFoldersViewModel> ojbListAll, TEMFoldersViewModel itemParent)
        {            
            List<TEMFoldersViewModel> ojbListResponse = new List<TEMFoldersViewModel>();
            var ojbListParent = ojbListAll.Where(x => x.Parent_Id == itemParent.Id).OrderBy(x => x.Code);
            if (ojbListParent != null)
            {
                foreach (TEMFoldersViewModel itemRun in ojbListParent)
                {
                    TEMFoldersViewModel item = (TEMFoldersViewModel)itemRun;
                    item.ParentName = itemParent.Name;
                    item.itemSub = GetAllActiveTreSubAsync(ojbListAll, item);
                    ojbListResponse.Add(item);
                }
                return ojbListResponse;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region "Select Default Table"
        public async Task<Response> GetByFilterAsync(TEMFoldersQueryModel filterModel)
        {
            try
            {
                var procName = "PKG_TEM_FOLDER.GET_BY_FILTER";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("pPageSize", filterModel.PageSize, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pPageIndex", filterModel.PageIndex, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pFullTextSearch", filterModel.FullTextSearch, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pName", (string.IsNullOrEmpty(filterModel.Name)) ? null : filterModel.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("pFolderId",filterModel.FolderId, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("pStatus", (string.IsNullOrEmpty(filterModel.Status)) ? null : filterModel.Status, OracleMappingType.Varchar2, ParameterDirection.Input);

                return await _TEMFoldersVViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetAllActiveAsync(string type="")
        {
            try
            {
                var procName = "PKG_TEM_FOLDER.GET_ALL_ACTIVE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PTYPE", type, OracleMappingType.NChar, ParameterDirection.Output);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                var itm = await _TEMFoldersVViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true) as ResponseObject<List<TEMFoldersViewModel>>;
                if (itm != null && itm.Data != null)
                {
                    List<TEMFoldersViewModel> ojbList = itm.Data;
                    if (ojbList != null && ojbList.Count > 0)
                    {
                        return new ResponseObject<List<TEMFoldersViewModel>>(ojbList, "Thành công", StatusCode.Success);
                    }
                    else
                    {
                        return new ResponseObject<List<TEMFoldersViewModel>>(null, "Không có dữ liệu", StatusCode.Success);
                    }
                }
                else
                {
                    return new ResponseObject<List<TEMFoldersViewModel>>(null, "Không có dữ liệu", StatusCode.Success);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetAllAsync(string Type)
        {
            try
            {
                var procName = "PKG_TEM_FOLDER.GET_ALL_ACTIVE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PTYPE", Type, OracleMappingType.Varchar2, ParameterDirection.Input);

                return await _TEMFoldersRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetListByUserPermisisonAsync(decimal userId)
        {
            try
            {
                var procName = "PKG_TEM_FOLDER.GET_LIST_ACTIVE_USER_PER";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PUSER_ID", userId, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMFoldersVViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, true);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> GetById(decimal id)
        {
            try
            {
                var procName = "PKG_TEM_FOLDER.GET_BY_ID";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                return await _TEMFoldersVViewRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        #endregion

        #region "CRU Default Table"
        public async Task<Response> CreateAsync(TEMFoldersCreateModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await CheckCodeAsync(0,model.Code) as ResponseObject<TEMFoldersViewModel>;
                if (objQuery != null && objQuery.Data != null)
                {
                    return new ResponseError(StatusCode.Fail, "Mã code đã tồn tại");
                }
                var procName = "PKG_TEM_FOLDER.INSERT_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PCODE ", model.Code, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PNAME ", model.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PPARENT_ID ", model.ParentId, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("PCREATEBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMFoldersRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> UpdateAsync(TEMFoldersUpdateModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetById(model.Id) as ResponseObject<TEMFoldersViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                     objQuery = await CheckCodeAsync(model.Id, model.Code) as ResponseObject<TEMFoldersViewModel>;
                    if (objQuery != null && objQuery.Data != null)
                    {
                        return new ResponseError(StatusCode.Fail, "Mã code đã tồn tại");
                    }

                    var procNam = "PKG_TEM_FOLDER.UPDATE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PCODE ", model.Code, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PNAME ", model.Name, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("PPARENT_ID ", model.ParentId, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PSTATUS", model.Status, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("LASTMODIFIEDBY", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMFoldersRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteAsync(decimal id, BaseModel baseModel)
        {
            try
            {
                var procName = "PKG_TEM_FOLDER.DELETE_ROW";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                return await _TEMFoldersRepositoryHandler.ExecuteProcOracle(procName, dyParam);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> DeleteManyAsync(List<decimal> listId, BaseModel baseModel)
        {
            var listResult = new List<ResponseModel>();
            using (var unitOfWorkOracle = new UnitOfWorkOracle())
            {
                var iConn = unitOfWorkOracle.GetConnection();
                var iTrans = iConn.BeginTransaction();
                if (listId != null && listId.Count > 0)
                {
                    foreach (var id in listId)
                    {
                        var procName = "PKG_TEM_FOLDER.DELETE_ROW";
                        var dyParam = new OracleDynamicParameters();
                        dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                        dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                        dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);
                        var deleteResult = await _TEMFoldersRepositoryHandler.ExecuteProcOracle(procName, iConn, iTrans, dyParam) as ResponseObject<ResponseModel>;

                        if (deleteResult != null)
                        {
                            listResult.Add(new ResponseModel
                            {
                                Id = deleteResult.Data.Id,
                                Name = deleteResult.Data.Name,
                                Status = deleteResult.Data.Status,
                                Message = (deleteResult.Data.Status.Equals("00") ? "Thành công" : "Không thành công")
                            });
                        }
                    }
                    iTrans.Commit();
                    return new ResponseObject<List<ResponseModel>>(listResult, "Thành công");
                }
                else
                {
                    return new ResponseObject<List<ResponseModel>>(listResult, "Không thành công");
                }

            }

        }
        public async Task<Response> ApprovedAsync(TEMFoldersApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetById(model.Id) as ResponseObject<TEMFoldersViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_FOLDER.APPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMFoldersRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> NoApprovedAsync(TEMFoldersApprovedModel model, BaseModel baseModel)
        {
            try
            {
                var objQuery = await GetById(model.Id) as ResponseObject<TEMFoldersViewModel>;

                if (objQuery == null || objQuery.Data == null)
                {
                    return new ResponseError(StatusCode.Fail, "Bản ghi không tồn tại");
                }
                else
                {
                    var procNam = "PKG_TEM_FOLDER.NOAPPROVE_ROW";
                    var dyParam = new OracleDynamicParameters();
                    dyParam.Add("PID", model.Id, OracleMappingType.Decimal, ParameterDirection.Input);
                    dyParam.Add("PMAKER", baseModel.Maker, OracleMappingType.Varchar2, ParameterDirection.Input);
                    dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                    return await _TEMFoldersRepositoryHandler.ExecuteProcOracle(procNam, dyParam);
                }
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        public async Task<Response> CheckCodeAsync(decimal id, string name)
        {
            try
            {
                var procName = "PKG_TEM_FOLDER.CHECK_BY_CODE";
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("PID", id, OracleMappingType.Decimal, ParameterDirection.Input);
                dyParam.Add("PCODE", name, OracleMappingType.Varchar2, ParameterDirection.Input);
                dyParam.Add("OUT_CUR", null, OracleMappingType.RefCursor, ParameterDirection.Output);

                return await _TEMFoldersRepositoryHandler.ExecuteProcOracleReturnRow(procName, dyParam, false);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.LogError(ex, "Exception Error");
                    return new ResponseError(StatusCode.Fail, "Lỗi ngoại lệ");
                }
                else throw ex;
            }
        }
        #endregion
    }
}
