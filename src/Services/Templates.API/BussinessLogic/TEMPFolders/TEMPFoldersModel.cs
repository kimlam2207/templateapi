﻿using System;
using System.Collections.Generic;
using Utils;

namespace Templates.API.BussinessLogic
{
    public class TEMFoldersBaseModel : BaseCRUDModel
    {
        public decimal Id { get; set; }
        public decimal Parent_Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }

        public string StatusView { get; set; }
        public decimal? TotalPage { get; set; }
        public decimal? TotalRecord { get; set; }
    }
    public class TEMFoldersViewModel : TEMFoldersBaseModel
    {
        public List<TEMFoldersViewModel> itemSub { get; set; }
    }

    public class TEMFoldersCreateModel 
    {
        public decimal ParentId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }

    public class TEMFoldersUpdateModel 
    {
        public decimal Id { get; set; }
        public decimal ParentId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
    public class TEMFoldersDeleteModel
    {
        public List<decimal> ListId { get; set; }
    }
    public class TEMFoldersApprovedModel
    {
        public decimal Id { get; set; }
    }
    public class TEMFoldersQueryModel : PaginationRequest
    {
        public string Name { get; set; }
        public decimal? FolderId { get; set; }
        public string Status { get; set; }
    }
}
