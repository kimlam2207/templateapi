﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Templates.API.Velocity;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using Spire.Doc;
using Microsoft.Extensions.Logging;

namespace Templates.API.Controllers
{
    [Route("api/temp/ecm")]
    [ApiController]
    public class ECMController : ControllerBase
    {

        private readonly IECMHandler _ECMHandler;
        private readonly ILogger<TEMFoldersHandler> _logger;

        public ECMController(IECMHandler ECMHandler, ILogger<TEMFoldersHandler> logger)       
        {
            _ECMHandler = ECMHandler;
            _logger = logger;
        }

        /// <summary>
        /// Download Template
        /// </summary>
        /// <param name="templateVersionId">Mã version</param>
        /// <param name="docId">Docid của ECM</param>
        /// <returns></returns>
        [HttpGet]
        [Route("download")]
        [ProducesResponseType(typeof(ResponseObject<ResponseECM>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DownloadAsync(string docId)
        {
            ResponseECM result = await _ECMHandler.DownloadFileAsync(docId);
            if (result!=null && result.StatusCode == Utils.StatusCode.Success)
            {
                byte[] bytes = Convert.FromBase64String(result.properties[0][0].docContent);
                var memoryStream = new MemoryStream(bytes);
                memoryStream.Position = 0; //let's rewind it
                memoryStream.Seek(0, SeekOrigin.Begin);
                return File(memoryStream, result.properties[0][0].fileType, result.properties[0][0].docName);
            }
            ResponseObject<ResponseECM> response = new ResponseObject<ResponseECM>(result);
            if (result.StatusCode == Utils.StatusCode.Fail)
            {
                response.StatusCode = Utils.StatusCode.Fail;
                response.Message = result.Message;
                response.Data = null;
                _logger.LogError(result.Message, "Exception Error");
            }
            return RequestHelpers.TransformData(response);
        }

        /// <summary>
        /// Replace bookmark word
        /// </summary>
        /// <param name="templateVersionId">Mã version</param>
        /// <param name="docId">Docid của ECM</param>
        /// <returns></returns
        /// 
        [HttpPost]
        [Route("renderfile")]
        [ProducesResponseType(typeof(ResponseObject<ResponseECM>), StatusCodes.Status200OK)]
        public async Task<IActionResult> RenderFileAsync([FromBody] TEMTemplatesReplaceDataModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            ResponseECM result = await _ECMHandler.RenderFileAsync(model, baseModel);        
            ResponseObject<ResponseECM> response = new ResponseObject<ResponseECM>(result);
            if (result.StatusCode == Utils.StatusCode.Fail)
            {
                response.StatusCode = Utils.StatusCode.Fail;
                response.Message = result.Message;
                response.Data = null;
            }
            else
            {
                response.Data.ResponseFile = null;
            }
            return RequestHelpers.TransformData(response);
        }
    }
}