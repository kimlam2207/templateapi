﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;

namespace Templates.API.Controllers
{
    [Route("api/temp/Folders")]
    [ApiController]
    public class TEMFoldersController : ControllerBase
    {
        private readonly ITEMFoldersHandler _TEMFoldersInterfaceHandler;
        public TEMFoldersController(ITEMFoldersHandler employeeInterfaceHandler)
        {
            _TEMFoldersInterfaceHandler = employeeInterfaceHandler;
        }

        #region "Other for Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMFolders theo dạng cây thư mục
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("activeviewtree")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllActiveTreAsync()
        {
            var result = await _TEMFoldersInterfaceHandler.GetAllActiveTreAsync();
            return RequestHelpers.TransformData(result);
        }
        #endregion

        #region "Select Default Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMFolders
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("active")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllActiveAsync()
        {
            var result = await _TEMFoldersInterfaceHandler.GetAllActiveAsync();
            return RequestHelpers.TransformData(result);
        }
       

        /// <summary>
        /// Lấy dữ liệu theo filter trong TEMFolders
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByFilterAsync(string filterModel)
        {
            TEMFoldersQueryModel model = JsonConvert.DeserializeObject<TEMFoldersQueryModel>(filterModel);
            var result = await _TEMFoldersInterfaceHandler.GetByFilterAsync(model);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin TEMFolders
        /// </summary>
        /// <param name="Type">Nếu Type=ALL: Lấy tất cả thông tin, còn không thì sẽ lấy trạng thái Atcive</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync(string Type)
        {
            var result = await _TEMFoldersInterfaceHandler.GetAllAsync(Type);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy  thông tin những bản ghi được phân quyền  TEMFolders 
        /// </summary>
        /// <param name="Type">Nếu Type=ALL: Lấy tất cả thông tin, còn không thì sẽ lấy trạng thái Atcive</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyuserpermission")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetListByUserPermisisonAsync(decimal userId)
        {
            var result = await _TEMFoldersInterfaceHandler.GetListByUserPermisisonAsync(userId);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy bản ghi theo id trong TEMFolders
        /// </summary>
        /// <param name="id">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _TEMFoldersInterfaceHandler.GetById(id);
            return RequestHelpers.TransformData(result);
        }


        #endregion

        #region "CRU Default Table"

        /// <summary>
        /// Thêm mới TEMFolders
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] TEMFoldersCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMFoldersInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật TEMFolders
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync([FromBody] TEMFoldersUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMFoldersInterfaceHandler.UpdateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa TEMFolders
        /// </summary>
        /// <param name="id">Mã ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMFoldersInterfaceHandler.DeleteAsync(id, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete TEMFolders many
        /// </summary>
        /// <param name="listId">danh sách mã IDs</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletemany")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteManyAsync(List<decimal> listId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMFoldersInterfaceHandler.DeleteManyAsync(listId, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Duyệt folder
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("approved")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ApprovedAsync(TEMFoldersApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMFoldersInterfaceHandler.ApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }


        /// <summary>
        /// Không Duyệt folder
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("noapproved")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> NoApprovedAsync(TEMFoldersApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMFoldersInterfaceHandler.NoApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }


        /// <summary>
        /// Check Code trong TEMFolders
        /// </summary>
        /// <param name="id">mã</param>
        /// <param name="code">Code</param>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckCode")]
        [ProducesResponseType(typeof(ResponseObject<TEMFoldersBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckCodeAsync(decimal id, string code)
        {
            var result = await _TEMFoldersInterfaceHandler.CheckCodeAsync(id, code);
            return RequestHelpers.TransformData(result);
        }

        #endregion
    }
}