﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;

namespace Templates.API.Controllers
{
    [Route("api/temp/aplications")]
    [ApiController]
    public class TEMAplicationsController : ControllerBase
    {
        private readonly ITEMAplicationsHandler _TEMAplicationsInterfaceHandler;
        public TEMAplicationsController(ITEMAplicationsHandler _alicationsInterfaceHandler)
        {
            _TEMAplicationsInterfaceHandler = _alicationsInterfaceHandler;
        }


        #region "Select Default Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMAplications
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _TEMAplicationsInterfaceHandler.GetAllAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin active được sử dụng TEMAplications
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("active")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllActiveAsync()
        {
            var result = await _TEMAplicationsInterfaceHandler.GetAllActiveAsync();
            return RequestHelpers.TransformData(result);
        }


        /// <summary>
        /// Lấy bản ghi theo id trong TEMAplications
        /// </summary>
        /// <param name="id">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _TEMAplicationsInterfaceHandler.GetByIdAsync(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Check Code trong TEMAplications
        /// </summary>
        /// <param name="id">mã</param>
        /// <param name="code">Code</param>
        /// <returns></returns>
        [HttpGet]
        [Route("bycode")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckCodeAsync(decimal id, string code)
        {
            var result = await _TEMAplicationsInterfaceHandler.GetByCodeAsync(id, code);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy dữ liệu theo filter trong TEMAplications
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByFilterAsync(string filterModel)
        {
            TEMAplicationsQueryModel model = JsonConvert.DeserializeObject<TEMAplicationsQueryModel>(filterModel);
            var result = await _TEMAplicationsInterfaceHandler.GetByFilterAsync(model);
            return RequestHelpers.TransformData(result);
        }

        #endregion

        #region "CRU Default Table"

        /// <summary>
        /// Thêm mới TEMAplications
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] TEMAplicationsCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMAplicationsInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật TEMAplications
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync([FromBody] TEMAplicationsUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMAplicationsInterfaceHandler.UpdateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa TEMAplications
        /// </summary>
        /// <param name="id">Mã ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMAplicationsInterfaceHandler.DeleteAsync(id, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete TEMAplications many
        /// </summary>
        /// <param name="listId">danh sách mã IDs</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletemany")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteManyAsync(List<decimal> listId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMAplicationsInterfaceHandler.DeleteManyAsync(listId, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Duyệt TEMAplications
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("approved")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ApprovedAsync(TEMAplicationsApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMAplicationsInterfaceHandler.ApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Không duyệt TEMAplications
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("noapproved")]
        [ProducesResponseType(typeof(ResponseObject<TEMAplicationsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> NoApprovedAsync(TEMAplicationsApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMAplicationsInterfaceHandler.NoApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }
        #endregion
    }
}