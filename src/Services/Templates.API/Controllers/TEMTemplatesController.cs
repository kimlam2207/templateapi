﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;
using System.IO;

namespace Templates.API.Controllers
{
    [Route("api/temp/templates")]
    [ApiController]
    public class TEMTemplatesController : ControllerBase
    {
        private readonly ITEMTemplatesHandler _TEMTemplatesInterfaceHandler;
        private readonly ITEMTemplatesTypeHandler _TEMTemplatesTypeHandler;
        private readonly ITEMTemplateRenderLogsHandler _TEMTemplateRenderLogsHandler;
        private readonly ITEMTemplateVersionsHandler _TEMTemplateVersionsInterfaceHandler;
        private readonly ITEMTemplateTypeMapsHandler _TEMTemplateTypeMapsInterfaceHandler;
        private readonly IECMHandler _ECMHandler;

        public TEMTemplatesController(ITEMTemplatesHandler _templatesInterfaceHandler,
                                        ITEMTemplatesTypeHandler templatesTypeHandler,
                                        ITEMTemplateRenderLogsHandler templateRenderLogsHandler,
                                        ITEMTemplateVersionsHandler templateVersionsInterfaceHandler,
                                        ITEMTemplateTypeMapsHandler templateTypeMapsInterfaceHandler,
                                        IECMHandler eCMHandler
)
        {
            _TEMTemplatesInterfaceHandler = _templatesInterfaceHandler;
            _TEMTemplatesTypeHandler = templatesTypeHandler;
            _TEMTemplateRenderLogsHandler = templateRenderLogsHandler;
            _TEMTemplateVersionsInterfaceHandler = templateVersionsInterfaceHandler;
            _TEMTemplateTypeMapsInterfaceHandler = templateTypeMapsInterfaceHandler;
            _ECMHandler = eCMHandler;
        }


        #region "Select Default Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _TEMTemplatesInterfaceHandler.GetAllAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin active được sử dụng TEMTemplates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("active")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllActiveAsync()
        {
            var result = await _TEMTemplatesInterfaceHandler.GetAllActiveAsync();
            return RequestHelpers.TransformData(result);
        }


        /// <summary>
        /// Lấy bản ghi theo id trong TEMTemplates
        /// </summary>
        /// <param name="id">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _TEMTemplatesInterfaceHandler.GetByIdAsync(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Check Code trong TEMTemplates
        /// </summary>
        /// <param name="id">mã</param>
        /// <param name="code">Code</param>
        /// <returns></returns>
        [HttpGet]
        [Route("bycode")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckCodeAsync(decimal id, string code)
        {
            var result = await _TEMTemplatesInterfaceHandler.GetByCodeAsync(id, code);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy dữ liệu theo filter trong TEMTemplates
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByFilterAsync(string filterModel)
        {
            TEMTemplatesQueryModel model = JsonConvert.DeserializeObject<TEMTemplatesQueryModel>(filterModel);
            var result = await _TEMTemplatesInterfaceHandler.GetByFilterAsync(model);
            return RequestHelpers.TransformData(result);
        }

        #endregion

        #region "CRU Default Table"

        /// <summary>
        /// Thêm mới TEMTemplates
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromForm] TEMTemplatesCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật TEMTemplates
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync([FromForm] TEMTemplatesUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesInterfaceHandler.UpdateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa TEMTemplates
        /// </summary>
        /// <param name="id">Mã ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesInterfaceHandler.DeleteAsync(id, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete TEMTemplates many
        /// </summary>
        /// <param name="listId">danh sách mã IDs</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletemany")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteManyAsync(List<decimal> listId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesInterfaceHandler.DeleteManyAsync(listId, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Duyệt TEMTemplates
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("approved")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ApprovedAsync(TEMTemplatesApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesInterfaceHandler.ApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Không duyệt Duyệt TEMTemplates
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("noapproved")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> NoApprovedAsync(TEMTemplatesNoApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesInterfaceHandler.NoApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Replace bookmark word
        /// </summary>
        /// <param name="templateVersionId">Mã version</param>
        /// <param name="docId">Docid của ECM</param>
        /// <returns></returns
        /// 
        [HttpPost]
        [Route("renderfile")]
        [ProducesResponseType(typeof(ResponseObject<ResponseECM>), StatusCodes.Status200OK)]
        public async Task<IActionResult> RenderFileAsync([FromBody] TEMTemplatesReplaceDataNewModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            ResponseECM result = await _TEMTemplatesInterfaceHandler.RenderFileAsync(model, baseModel, _ECMHandler);
            ResponseObject<ResponseECM> response = new ResponseObject<ResponseECM>(result);
            if (result.StatusCode==Utils.StatusCode.Fail)
            {
                response.StatusCode = Utils.StatusCode.Fail;
                response.Message = result.Message;
                response.Data = null;
            }
            else
            {
                response.Data.ResponseFile = null;
            }
            return RequestHelpers.TransformData(response);
        }

        /// <summary>
        /// Download Template
        /// </summary>
        /// <param name="templateVersionId">Mã version</param>
        /// <param name="docId">Docid của ECM</param>
        /// <returns></returns>
        [HttpGet]
        [Route("download")]
        [ProducesResponseType(typeof(ResponseObject<ResponseECM>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DownloadAsync(string docId)
        {
            ResponseECM result = await _ECMHandler.DownloadFileAsync(docId);
            if (result != null && result.StatusCode == Utils.StatusCode.Success)
            {
                byte[] bytes = Convert.FromBase64String(result.properties[0][0].docContent);
                var memoryStream = new MemoryStream(bytes);
                memoryStream.Position = 0; //let's rewind it
                memoryStream.Seek(0, SeekOrigin.Begin);
                return File(memoryStream, result.properties[0][0].fileType, result.properties[0][0].docName);
            }
            ResponseObject<ResponseECM> response = new ResponseObject<ResponseECM>(result);
            if (result.StatusCode == Utils.StatusCode.Fail)
            {
                response.StatusCode = Utils.StatusCode.Fail;
                response.Message = result.Message;
                response.Data = null;
            }
            return RequestHelpers.TransformData(response);
        }
        #endregion
    }
}