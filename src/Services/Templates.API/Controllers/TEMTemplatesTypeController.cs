﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;

namespace Templates.API.Controllers
{
    [Route("api/temp/TemplatesType")]
    [ApiController]
    public class TEMTemplatesTypeController : ControllerBase
    {
        private readonly ITEMTemplatesTypeHandler _TEMTemplatesTypeInterfaceHandler;
        public TEMTemplatesTypeController(ITEMTemplatesTypeHandler templatesTypeInterfaceHandler)
        {
            _TEMTemplatesTypeInterfaceHandler = templatesTypeInterfaceHandler;
        }


        #region "Select Default Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplateTypes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _TEMTemplatesTypeInterfaceHandler.GetAllAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplateTypes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("active")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllActiveAsync()
        {
            var result = await _TEMTemplatesTypeInterfaceHandler.GetAllActiveAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy bản ghi theo id trong TEMTemplateTypes
        /// </summary>
        /// <param name="id">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _TEMTemplatesTypeInterfaceHandler.GetByIdAsync(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Check Code trong TEMTemplateTypes
        /// </summary>
        /// <param name="id">mã</param>
        /// <param name="code">Code</param>
        /// <returns></returns>
        [HttpGet]
        [Route("bycode")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckCodeAsync(decimal id, string code)
        {
            var result = await _TEMTemplatesTypeInterfaceHandler.GetByCodeAsync(id, code);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy dữ liệu theo filter trong TEMTemplateTypes
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByFilterAsync(string filterModel)
        {
            TEMTemplateTypesQueryModel model = JsonConvert.DeserializeObject<TEMTemplateTypesQueryModel>(filterModel);
            var result = await _TEMTemplatesTypeInterfaceHandler.GetByFilterAsync(model);
            return RequestHelpers.TransformData(result);
        }
        #endregion

        #region "CRU Default Table"

        /// <summary>
        /// Thêm mới TEMTemplateTypes
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] TEMTemplateTypesCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesTypeInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật TEMTemplateTypes
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync([FromBody] TEMTemplateTypesUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesTypeInterfaceHandler.UpdateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa TEMTemplateTypes
        /// </summary>
        /// <param name="id">Mã ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesTypeInterfaceHandler.DeleteAsync(id, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete TEMTemplateTypes many
        /// </summary>
        /// <param name="listId">danh sách mã IDs</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletemany")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteManyAsync(List<decimal> listId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesTypeInterfaceHandler.DeleteManyAsync(listId, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Duyệt TEMTemplateTypes
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("approved")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ApprovedAsync(TEMTemplateTypesApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesTypeInterfaceHandler.ApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Không Duyệt TEMTemplateTypes
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("noapproved")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> NoApprovedAsync(TEMTemplateTypesApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplatesTypeInterfaceHandler.NoApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }
        #endregion
    }
}