﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;

namespace Templates.API.Controllers
{
    [Route("api/temp/TemplateRenderLogs")]
    [ApiController]
    public class TEMTemplateRenderLogsController : ControllerBase
    {
        private readonly ITEMTemplateRenderLogsHandler _TEMTemplateRenderLogsInterfaceHandler;
        public TEMTemplateRenderLogsController(ITEMTemplateRenderLogsHandler _alicationsInterfaceHandler)
        {
            _TEMTemplateRenderLogsInterfaceHandler = _alicationsInterfaceHandler;
        }


        #region "Select Default Table"
        /// <summary>
        /// Lấy dữ liệu theo filter trong TEMTemplateRenderLogs
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateRenderLogsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByFilterAsync(string filterModel)
        {
            TEMTemplateRenderLogsQueryModel model = JsonConvert.DeserializeObject<TEMTemplateRenderLogsQueryModel>(filterModel);
            var result = await _TEMTemplateRenderLogsInterfaceHandler.GetByFilterAsync(model);
            return RequestHelpers.TransformData(result);
        }
        #endregion


    }
}