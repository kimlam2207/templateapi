﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;

namespace Templates.API.Controllers
{
    [Route("api/temp/TemplateAplicationPermission")]
    [ApiController]
    public class TEMTemplateAplicationPermissionsController : ControllerBase
    {
        private readonly ITEMTemplateAplicationPermissionsHandler _TEMTemplateAplicationPermissionsInterfaceHandler;
        public TEMTemplateAplicationPermissionsController(ITEMTemplateAplicationPermissionsHandler _alicationsInterfaceHandler)
        {
            _TEMTemplateAplicationPermissionsInterfaceHandler = _alicationsInterfaceHandler;
        }


        #region "Select Default Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplateAplicationPermissions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateAplicationPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _TEMTemplateAplicationPermissionsInterfaceHandler.GetAllAsync();
            return RequestHelpers.TransformData(result);
        }


        /// <summary>
        /// Lấy bản ghi theo id trong TEMTemplateAplicationPermissions
        /// </summary>
        /// <param name="id">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateAplicationPermissionsBaseModel>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _TEMTemplateAplicationPermissionsInterfaceHandler.GetByIdAsync(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Check Code trong TEMTemplateAplicationPermissions
        /// </summary>
        /// <param name="templateId">mã template</param>
        /// <param name="aplicationId">mã ứng dụng</param>
        /// <returns></returns>
        [HttpGet]
        [Route("bytemplateIdorapplicationId")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateAplicationPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByTemplateIdOrAplicationIdAsync(decimal templateId, decimal aplicationId)
        {
            var result = await _TEMTemplateAplicationPermissionsInterfaceHandler.GetByTemplateIdOrAplicationIdAsync(templateId, aplicationId);
            return RequestHelpers.TransformData(result);
        }
        #endregion

        #region "CRU Default Table"

        /// <summary>
        /// Thêm mới TEMTemplateAplicationPermissions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateAplicationPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] TEMTemplateAplicationPermissionsCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateAplicationPermissionsInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật TEMTemplateAplicationPermissions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateAplicationPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync([FromBody] TEMTemplateAplicationPermissionsUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateAplicationPermissionsInterfaceHandler.UpdateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa TEMTemplateAplicationPermissions
        /// </summary>
        /// <param name="id">Mã ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateAplicationPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateAplicationPermissionsInterfaceHandler.DeleteAsync(id, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete TEMTemplateAplicationPermissions many
        /// </summary>
        /// <param name="listId">danh sách mã IDs</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletemany")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateAplicationPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteManyAsync(List<decimal> listId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateAplicationPermissionsInterfaceHandler.DeleteManyAsync(listId, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cre list TEMTemplateAplicationPermissions 
        /// </summary>
        /// <param name="Aplications">danh sách mã aplication id</param>
        /// <param name="TemplateId">Mã template </param>
        /// <returns></returns>
        [HttpPost]
        [Route("createlist")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateAplicationPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateListAsync([FromBody] TEMTemplateAplicationPermissionsListModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateAplicationPermissionsInterfaceHandler.CreateListAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }
        #endregion
    }
}