﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;

namespace Templates.API.Controllers
{
    [Route("api/temp/TemplateTypeMaps")]
    [ApiController]
    public class TEMTemplateTypeMapsController : ControllerBase
    {
        private readonly ITEMTemplateTypeMapsHandler _TEMTemplateTypeMapsInterfaceHandler;
        public TEMTemplateTypeMapsController(ITEMTemplateTypeMapsHandler _alicationsInterfaceHandler)
        {
            _TEMTemplateTypeMapsInterfaceHandler = _alicationsInterfaceHandler;
        }


        #region "Select Default Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplateTypeMaps
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypeMapsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _TEMTemplateTypeMapsInterfaceHandler.GetAllAsync();
            return RequestHelpers.TransformData(result);
        }


        /// <summary>
        /// Lấy bản ghi theo id trong TEMTemplateTypeMaps
        /// </summary>
        /// <param name="id">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypeMapsBaseModel>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _TEMTemplateTypeMapsInterfaceHandler.GetByIdAsync(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy danh sách map template vs loại template TEMTemplateTypeMaps
        /// </summary>
        /// <param name="templateId">mã template</param>
        /// <returns></returns>
        [HttpGet]
        [Route("bytemplateid")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypeMapsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByTemplateIdAsync(decimal templateId)
        {
            var result = await _TEMTemplateTypeMapsInterfaceHandler.GetByTemplateIdAsync(templateId);
            return RequestHelpers.TransformData(result);
        }
        #endregion

        #region "CRU Default Table"

        /// <summary>
        /// Thêm mới TEMTemplateTypeMaps
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypeMapsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] TEMTemplateTypeMapsCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateTypeMapsInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật TEMTemplateTypeMaps
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypeMapsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync([FromBody] TEMTemplateTypeMapsUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateTypeMapsInterfaceHandler.UpdateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa TEMTemplateTypeMaps
        /// </summary>
        /// <param name="id">Mã ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypeMapsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateTypeMapsInterfaceHandler.DeleteAsync(id, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete TEMTemplateTypeMaps many
        /// </summary>
        /// <param name="listId">danh sách mã IDs</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletemany")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypeMapsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteManyAsync(List<decimal> listId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateTypeMapsInterfaceHandler.DeleteManyAsync(listId, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cre list TEMTemplateTypeMaps 
        /// </summary>
        /// <param name="TemplateTypes">danh sách mã template type id</param>
        /// <param name="Template_Id">Mã </param>
        /// <returns></returns>
        [HttpPost]
        [Route("createlist")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateTypeMapsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateListAsync([FromBody] TEMTemplateTypeMapsCreateListModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateTypeMapsInterfaceHandler.CreateListAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }
        #endregion
    }
}