﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using UTILS.API.BussinessLogic;

namespace UTILS.API.Controllers
{
    [Route("api/utils/links")]
    [ApiController]
    public class LinksController : ControllerBase
    {
        private readonly ILinksHandler _LinksInterfaceHandler;
        public LinksController(ILinksHandler employeeInterfaceHandler)
        {
            _LinksInterfaceHandler = employeeInterfaceHandler;
        }

        #region GET

        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("active")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllActiveAsync()
        {
            var result = await _LinksInterfaceHandler.GetAllActiveAsync();
            return RequestHelpers.TransformData(result);
        }
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("bygrounlinksid")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetActiveByGroupLinksIDAsync(decimal GroupLinksId)
        {
            var result = await _LinksInterfaceHandler.GetActiveByGroupLinksIDAsync(GroupLinksId);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("bygrounlinksall")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetActiveByGroupLinksALLAsync()
        {
            var result = await _LinksInterfaceHandler.GetActiveByGroupLinksALLAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy dữ liệu theo filter
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByFilterAsync(string filterModel)
        {
            LinksQueryModel model = JsonConvert.DeserializeObject<LinksQueryModel>(filterModel);
            var result = await _LinksInterfaceHandler.GetByFilterAsync(model);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _LinksInterfaceHandler.GetAllAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy theo bản ghi 
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _LinksInterfaceHandler.GetById(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// kiem tra ten
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("checkname")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckNameAsync(decimal id, string name)
        {
            var result = await _LinksInterfaceHandler.CheckNameAsync(id,name);
            return RequestHelpers.TransformData(result);
        }

        #endregion

        #region CRUD
        /// <summary>
        /// Thêm mới Links
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] LinksCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _LinksInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật Links
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync(decimal id, [FromBody] LinksUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _LinksInterfaceHandler.UpdateAsync(id, model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa Links
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _LinksInterfaceHandler.DeleteAsync(id, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete Links many
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletemany")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteManyAsync(List<decimal> listId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _LinksInterfaceHandler.DeleteManyAsync(listId, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete Links many
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("approved")]
        [ProducesResponseType(typeof(ResponseObject<LinksBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ApprovedAsync(LinksApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _LinksInterfaceHandler.ApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }
        #endregion
    }
}