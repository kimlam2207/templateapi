﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;

namespace Templates.API.Controllers
{
    [Route("api/temp/TemplateVersions")]
    [ApiController]
    public class TEMTemplateVersionsController : ControllerBase
    {
        private readonly ITEMTemplateVersionsHandler _TEMTemplateVersionsInterfaceHandler;
        private readonly ITEMTemplatesHandler _templatesHandler;

        private readonly IRedisService _redisService;
        public TEMTemplateVersionsController(ITEMTemplateVersionsHandler _alicationsInterfaceHandler,
                                                IRedisService redisService,
                                                ITEMTemplatesHandler templatesHandler)
        {
            _TEMTemplateVersionsInterfaceHandler = _alicationsInterfaceHandler;
            _redisService = redisService;
            _templatesHandler = templatesHandler;
        }


        #region "Select Default Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplateVersions theo id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateVersions>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _TEMTemplateVersionsInterfaceHandler.GetByIdAsync(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplateVersions theo template id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("bytemplateid")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateVersions>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByTemplateIdAsync(decimal templateId)
        {
            var result = await _TEMTemplateVersionsInterfaceHandler.GetByTemplateIdAsync(templateId);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplateVersions theo template code
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("bytemplatecode")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateVersions>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByTemplateCodeAsync(string code)
        {
            var result = await _TEMTemplateVersionsInterfaceHandler.GetByTemplateCodeAsync(code);
            return RequestHelpers.TransformData(result);
        }
        #endregion

        #region "CRUD"
        /// <summary>
        /// Xóa TEMTemplates
        /// </summary>
        /// <param name="id">Mã ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplatesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateVersionsInterfaceHandler.DeleteAsync(id, baseModel,_templatesHandler);
            return RequestHelpers.TransformData(result);
        }
        #endregion


        #region "CRU Default Table"

        /// <summary>
        /// Thêm mới TEMTemplateVersions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateVersionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] TEMTemplateVersionsCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateVersionsInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật TEMTemplateVersions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateVersionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync([FromBody] TEMTemplateVersionsUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateVersionsInterfaceHandler.UpdateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        

        /// <summary>
        /// Duyệt folder
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("approved")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateVersionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ApprovedAsync(TEMTemplateVersionsApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateVersionsInterfaceHandler.ApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }


        /// <summary>
        /// Không Duyệt folder
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("noapproved")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateVersionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> NoApprovedAsync(TEMTemplateVersionsNoApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateVersionsInterfaceHandler.NoApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        #endregion
    }
}