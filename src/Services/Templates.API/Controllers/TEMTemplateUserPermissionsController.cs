﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Templates.API.BussinessLogic;

namespace Templates.API.Controllers
{
    [Route("api/temp/TemplateUserPermissions")]
    [ApiController]
    public class TEMTemplateUserPermissionsController : ControllerBase
    {
        private readonly ITEMTemplateUserPermissionsHandler _TEMTemplateUserPermissionsInterfaceHandler;
        public TEMTemplateUserPermissionsController(ITEMTemplateUserPermissionsHandler TemplateUserPermissionsInterfaceHandler)
        {
            _TEMTemplateUserPermissionsInterfaceHandler = TemplateUserPermissionsInterfaceHandler;
        }


        #region "Select Default Table"
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng TEMTemplateUserPermissions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.GetAllAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ bản ghi theo userId trong TEMTemplateUserPermissions
        /// </summary>
        /// <param name="userId">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("userId")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetByUserIdAsync(decimal userId)
        {
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.GetbyUserIdAsync(userId);
            return RequestHelpers.TransformData(result);
        }


        /// <summary>
        /// Lấy bản ghi theo id trong TEMTemplateUserPermissions
        /// </summary>
        /// <param name="id">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.GetById(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy dữ liệu theo filter trong TEMTemplateUserPermissions
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByFilterAsync(string filterModel)
        {
            TEMTemplateUserPermissionsQueryModel model = JsonConvert.DeserializeObject<TEMTemplateUserPermissionsQueryModel>(filterModel);
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.GetByFilterAsync(model);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy dữ liệu theo filter trong TEMTemplateUserPermissions
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("filter")]
        //[ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]
        //public async Task<IActionResult> GetByFilterAsync(string filterModel)
        //{
        //    TEMTemplateUserPermissionsQueryModel model = JsonConvert.DeserializeObject<TEMTemplateUserPermissionsQueryModel>(filterModel);
        //    var result = await _TEMTemplateUserPermissionsInterfaceHandler.GetByFilterAsync(model);
        //    return RequestHelpers.TransformData(result);
        //}
        #endregion

        #region "CRU Default Table"

        /// <summary>
        /// Thêm mới TEMTemplateUserPermissions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] List<TEMTemplateUserPermissionsCreateModel> models)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.CreateAsync(models, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật TEMTemplateUserPermissions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync([FromBody] List<TEMTemplateUserPermissionsUpdateModel> models)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.UpdateAsync(models, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa TEMTemplateUserPermissions bởi userId
        /// </summary>
        /// <param name="userId">Mã userid</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletebyuserid")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteByUserIdAsync(decimal userId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.DeleteByUserIdAsync(userId);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Duyệt TEMTemplateUserPermissions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("approved")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ApprovedAsync(TEMTemplateUserPermissionsApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.ApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Không Duyệt TEMTemplateUserPermissions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("noapproved")]
        [ProducesResponseType(typeof(ResponseObject<TEMTemplateUserPermissionsBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> NoApprovedAsync(TEMTemplateUserPermissionsApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _TEMTemplateUserPermissionsInterfaceHandler.NoApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }
        #endregion
    }
}