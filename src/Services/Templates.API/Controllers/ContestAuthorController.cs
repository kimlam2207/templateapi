﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using UTILS.API.BussinessLogic;

namespace UTILS.API.Controllers
{
    [Route("api/spring-publication")]
    [ApiController]
    public class ContestAuthorController : ControllerBase
    {
        private readonly IContestAuthorHandler _ContestAuthorInterfaceHandler;
        public ContestAuthorController(IContestAuthorHandler employeeInterfaceHandler)
        {
            _ContestAuthorInterfaceHandler = employeeInterfaceHandler;
        }

        #region GET
              
        /// <summary>
        /// Lấy toàn bộ thông tin
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponseObject<ContestAuthorModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _ContestAuthorInterfaceHandler.GetAllAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy theo bản ghi 
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<ContestAuthorModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _ContestAuthorInterfaceHandler.GetById(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy theo bản ghi 
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<ContestAuthorModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByTransactionDetailId(decimal TransactionDetailID)
        {
            var result = await _ContestAuthorInterfaceHandler.GetByTransactionDetailId(TransactionDetailID);
            return RequestHelpers.TransformData(result);
        }




        #endregion

        #region CRUD
        /// <summary>
        /// Thêm mới ContestAuthor
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<ContestAuthorModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] ContestAuthorModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var result = await _ContestAuthorInterfaceHandler.CreateAsync(model);
            return RequestHelpers.TransformData(result);
        }

        #endregion
    }
}