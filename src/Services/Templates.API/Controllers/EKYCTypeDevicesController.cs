﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using EKYCAdmin.API.BussinessLogic;

namespace EKYCAdmin.API.Controllers
{
    [Route("api/utils/ekyctypedevices")]
    [ApiController]
    public class EKYCTypeDevicesController : ControllerBase
    {
        private readonly IEKYCTypeDevicesHandler _EKYCTypeDevicesInterfaceHandler;
        public EKYCTypeDevicesController(IEKYCTypeDevicesHandler employeeInterfaceHandler)
        {
            _EKYCTypeDevicesInterfaceHandler = employeeInterfaceHandler;
        }

        #region GET

        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng EKYCTypeDevices
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("active")]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllActiveAsync()
        {
            var result = await _EKYCTypeDevicesInterfaceHandler.GetAllActiveAsync();
            return RequestHelpers.TransformData(result);
        }
        /// <summary>
        /// Lấy toàn bộ thông tin được sử dụng EKYCTypeDevices theo dạng cây thư mục
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("activeviewtree")]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllActiveTreAsync()
        {
            var result = await _EKYCTypeDevicesInterfaceHandler.GetAllActiveTreAsync();
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy dữ liệu theo filter trong EKYCTypeDevices
        /// </summary>
        /// <param name="model">model dung de filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByFilterAsync(string filterModel)
        {
            EKYCTypeDevicesQueryModel model = JsonConvert.DeserializeObject<EKYCTypeDevicesQueryModel>(filterModel);
            var result = await _EKYCTypeDevicesInterfaceHandler.GetByFilterAsync(model);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy toàn bộ thông tin EKYCTypeDevices
        /// </summary>
        /// <param name="Type">Nếu Type=ALL: Lấy tất cả thông tin, còn không thì sẽ lấy trạng thái Atcive</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync(string Type)
        {
            var result = await _EKYCTypeDevicesInterfaceHandler.GetAllAsync(Type);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Lấy bản ghi theo id trong EKYCTypeDevices
        /// </summary>
        /// <param name="id">mã</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(decimal id)
        {
            var result = await _EKYCTypeDevicesInterfaceHandler.GetById(id);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// kiem tra ten trong EKYCTypeDevices
        /// </summary>
        /// <param name="id">mã</param>
        /// <param name="code">Code</param>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckCode")]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckCodeAsync(decimal id, string code)
        {
            var result = await _EKYCTypeDevicesInterfaceHandler.CheckCodeAsync(id, code);
            return RequestHelpers.TransformData(result);
        }

        #endregion

        #region CRUD
        /// <summary>
        /// Thêm mới EKYCTypeDevices
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] EKYCTypeDevicesCreateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _EKYCTypeDevicesInterfaceHandler.CreateAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Cập nhật EKYCTypeDevices
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync(decimal id, [FromBody] EKYCTypeDevicesUpdateModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _EKYCTypeDevicesInterfaceHandler.UpdateAsync(id, model, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Xóa EKYCTypeDevices
        /// </summary>
        /// <param name="id">Mã ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(decimal id)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _EKYCTypeDevicesInterfaceHandler.DeleteAsync(id, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete EKYCTypeDevices many
        /// </summary>
        /// <param name="listId">danh sách mã IDs</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deletemany")]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteManyAsync(List<decimal> listId)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _EKYCTypeDevicesInterfaceHandler.DeleteManyAsync(listId, baseModel);
            return RequestHelpers.TransformData(result);
        }

        /// <summary>
        /// Delete EKYCTypeDevices many
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("approved")]
        [ProducesResponseType(typeof(ResponseObject<EKYCTypeDevicesBaseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> ApprovedAsync(EKYCTypeDevicesApprovedModel model)
        {
            var requestInfo = RequestHelpers.GetRequestInfo(Request);
            var baseModel = new BaseModel
            {
                Maker = requestInfo.UserName,
                MakerOnDate = DateTime.Now
            };
            var result = await _EKYCTypeDevicesInterfaceHandler.ApprovedAsync(model, baseModel);
            return RequestHelpers.TransformData(result);
        }
        #endregion
    }
}