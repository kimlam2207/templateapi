namespace API.Extensions
{
    using global::Templates.API.BussinessLogic;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.StackExchangeRedis;
    using Microsoft.Extensions.DependencyInjection;
    using Templates.API;

    /// <summary>
    /// <see cref="IServiceCollection"/> extension methods add project services.
    /// </summary>
    /// <remarks>
    /// AddSingleton - Only one instance is ever created and returned.
    /// AddScoped - A new instance is created and returned for each request/response cycle.
    /// AddTransient - A new instance is created and returned each time.
    /// </remarks>
    public static class ProjectServiceCollectionExtensions
    {
        public static IServiceCollection AddProjectServices(this IServiceCollection services) => services
            .AddSingleton<ITEMFoldersHandler, TEMFoldersHandler>()
            .AddSingleton<ITEMTemplatesTypeHandler, TEMTemplateTypesHandler>()
            .AddSingleton<ITEMAplicationsHandler, TEMAplicationsHandler>()
            .AddSingleton<ITEMTemplateTypeMapsHandler, TEMTemplateTypeMapsHandler>()
            .AddSingleton<ITEMTemplateAplicationPermissionsHandler, TEMTemplateAplicationPermissionsHandler>()
            .AddSingleton<ITEMTemplatesHandler, TEMTemplatesHandler>()
            .AddSingleton<ITEMTemplateVersionsHandler, TEMTemplateVersionsHandler>()
            .AddSingleton<ITEMTemplateRenderLogsHandler, TEMTemplateRenderLogsHandler>()
            .AddSingleton<ITEMTemplateUserPermissionsHandler, TEMTemplateUserPermissionsHandler>()
            .AddSingleton<IDistributedCache, RedisCache>()
            .AddSingleton<IRedisService, RedisService>()
            .AddSingleton<IECMHandler, ECMHandler>()
            ;  
    }
}